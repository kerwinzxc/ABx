/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "MissionMapWindow.h"
#include "Shortcuts.h"
#include "LevelManager.h"
#include "Player.h"
#include "WorldLevel.h"
#include "ServerEvents.h"
#include <sa/time.h>
#include "FwClient.h"
#include "AudioManager.h"
#include <abshared/Mechanic.h>
#include <sa/StringTempl.h>
#include "Conversions.h"

inline constexpr int MAP_WIDTH = 512;
inline constexpr int MAP_HEIGHT = 512;
// Pixel per Meter
inline constexpr int SCALE = 5;

static constexpr sa::color SELF_COLOR{ 77, 255, 77 };
static constexpr sa::color ALLY_COLOR{ 0, 179, 0 };
static constexpr sa::color FOE_COLOR{ 255, 0, 0 };
static constexpr sa::color OTHER_COLOR{ 0, 0, 255 };
static constexpr sa::color RED_TEAM_COLOR{ 255, 0, 0 };
static constexpr sa::color BLUE_TEAM_COLOR{ 0, 0, 255 };
static constexpr sa::color YELLOW_TEAM_COLOR{ 255, 255, 0 };

static constexpr sa::color WAYPOINT_COLOR{ 117, 18, 10 };
static constexpr sa::color PING_COLOR{ 230, 51, 204 };
static constexpr sa::color MARKER_COLOR{ 0, 128, 0 };
static constexpr sa::color AGGRO_RANGE_COLOR{ 230, 51, 51, 140 };
static constexpr sa::color CASTING_RANGE_COLOR{ 51, 230, 51, 140 };

void MissionMapWindow::RegisterObject(Context* context)
{
    context->RegisterFactory<MissionMapWindow>();
    URHO3D_COPY_BASE_ATTRIBUTES(Window);
}

String MissionMapWindow::GetMinimapFile(const String& scene)
{
    const std::string name = sa::CombinePath<char>("Textures/Minimaps", sa::ExtractFileName<char>(scene.CString())) + ".png";
    return ToUrhoString(name);
}

MissionMapWindow::MissionMapWindow(Context* context) :
    Window(context)
{
    SetName("MissionMapWindow");

    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    XMLFile* file = cache->GetResource<XMLFile>("UI/MissionMapWindow.xml");
    LoadXML(file->GetRoot());

    Shortcuts* scs = GetSubsystem<Shortcuts>();
    Text* caption = GetChildStaticCast<Text>("CaptionText", true);
    caption->SetText(scs->GetCaption(Events::E_SC_TOGGLEMISSIONMAPWINDOW, "Mission Map", true));

    auto* graphics = GetSubsystem<Graphics>();
    SetPosition(graphics->GetWidth() - GetWidth() - 5, graphics->GetHeight() / 2 - (GetHeight() / 2));

    SetStyleAuto();

    UpdateLayout();

    SubscribeToEvents();
}


MissionMapWindow::~MissionMapWindow()
{
    UnsubscribeFromAllEvents();
}

void MissionMapWindow::SetScene(SharedPtr<Scene> scene, AB::Entities::GameType gameType, const String& sceneFile)
{
    waypoints_.Clear();
    gameType_ = gameType;
    if (!scene)
        return;

    const String minimapFile = GetMinimapFile(sceneFile);
    auto* cache = GetSubsystem<ResourceCache>();
    // If this does not exist it falls back to the heightmap
    auto* minimapImage = cache->GetResource<Image>(minimapFile);

    terrainLayer_ = GetChildStaticCast<BorderImage>("Container", true);
    objectLayer_ = terrainLayer_->GetChildStaticCast<BorderImage>("ObjectLayer", true);
    auto* terrain = scene->GetComponent<Terrain>(true);
    if (terrain && terrain->GetHeightMap())
    {
        terrainSpacing_ = terrain->GetSpacing();
        auto* heightmap = terrain->GetHeightMap();
        heightmapTexture_ = MakeShared<Texture2D>(context_);
        heightmapTexture_->SetSize(heightmap->GetWidth(), heightmap->GetHeight(), Graphics::GetRGBAFormat(), TEXTURE_STATIC);
        if (minimapImage)
            heightmapTexture_->SetData(minimapImage, true);
        else
            heightmapTexture_->SetData(heightmap, true);
        heightmapTexture_->SetNumLevels(1);
        heightmapTexture_->SetMipsToSkip(QUALITY_LOW, 0);
        terrainLayer_->SetTexture(heightmapTexture_);
        terrainWorldSize_ = { (float)heightmap->GetWidth() * terrainSpacing_.x_,
            terrainSpacing_.y_,
            (float)heightmap->GetHeight() * terrainSpacing_.z_ };
        terrainScaling_ = { terrainWorldSize_.x_ / (float)MAP_WIDTH, terrainWorldSize_.z_ / (float)MAP_HEIGHT };
    }
    else
        terrainLayer_->SetTexture(nullptr);

    mapTexture_ = MakeShared<Texture2D>(context_);
    mapTexture_->SetSize(MAP_WIDTH, MAP_HEIGHT, Graphics::GetRGBAFormat(), TEXTURE_DYNAMIC);
    mapTexture_->SetNumLevels(1);
    mapTexture_->SetMipsToSkip(QUALITY_LOW, 0);
    mapImage_ = MakeShared<Image>(context_);
    mapImage_->SetSize(MAP_WIDTH, MAP_HEIGHT, 4);
    mapBitmap_.set_bitmap(MAP_WIDTH, MAP_HEIGHT, 4, mapImage_->GetData());
    objectLayer_->SetTexture(mapTexture_);
    objectLayer_->SetFullImageRect();
}

void MissionMapWindow::FitTexture()
{
    if (!mapTexture_)
        return;
}

void MissionMapWindow::SubscribeToEvents()
{
    Button* closeButton = GetChildStaticCast<Button>("CloseButton", true);
    SubscribeToEvent(closeButton, E_RELEASED, URHO3D_HANDLER(MissionMapWindow, HandleCloseClicked));
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(MissionMapWindow, HandleUpdate));
    SubscribeToEvent(E_RENDERUPDATE, URHO3D_HANDLER(MissionMapWindow, HandleRenderUpdate));
    SubscribeToEvent(this, E_RESIZED, URHO3D_HANDLER(MissionMapWindow, HandleResized));
    SubscribeToEvent(Events::E_POSITION_PINGED, URHO3D_HANDLER(MissionMapWindow, HandlePositionPinged));
    SubscribeToEvent(E_MOUSEBUTTONDOWN, URHO3D_HANDLER(MissionMapWindow, HandleMouseDown));
    SubscribeToEvent(E_MOUSEBUTTONUP, URHO3D_HANDLER(MissionMapWindow, HandleMouseUp));
    SubscribeToEvent(Events::E_OBJECTPINGTARGET, URHO3D_HANDLER(MissionMapWindow, HandleTargetPinged));
}

void MissionMapWindow::HandleCloseClicked(StringHash, VariantMap&)
{
    SetVisible(false);
}

IntVector2 MissionMapWindow::WorldToMapPos(const Vector3& center, const Vector3& world) const
{
    return WorldToMap(world - center);
}

IntVector2 MissionMapWindow::WorldToMap(const Vector3& world) const
{
    float x = (world.x_ * SCALE) + ((float)MAP_WIDTH * 0.5f);
    float y = (-world.z_ * SCALE) + ((float)MAP_HEIGHT * 0.5f);
    return { (int)x, (int)y };
}

Vector3 MissionMapWindow::MapToWorld(const IntVector2& map) const
{
    static const Vector3 offset = {
        (float)MAP_WIDTH * 0.5f, 0.0f, (float)MAP_HEIGHT * 0.5f
    };
    const Vector3 scaling = { (float)terrainLayer_->GetSize().x_ / (float)MAP_WIDTH,
        1.0f,
        (float)terrainLayer_->GetSize().y_ / (float)MAP_HEIGHT };

    const Vector3 pos = Vector3((float)map.x_ / scaling.x_, 0.0f, (float)map.y_ / scaling.z_) - offset;

    return pos / SCALE;
}

Vector3 MissionMapWindow::MapToWorldPos(const Vector3& center, const IntVector2& map) const
{
    return center + MapToWorld(map);
}

void MissionMapWindow::DrawPoint(const IntVector2& center, int size, const sa::color& color)
{
    mapBitmap_.draw_point(center.x_, center.y_, size, color);
}

void MissionMapWindow::DrawCircle(const IntVector2& center, float radius, const sa::color& color, int thickness)
{
    mapBitmap_.draw_circle(center.x_, center.y_, radius * (float)SCALE, color, thickness);
}

void MissionMapWindow::DrawCircle(const IntVector2& center, float radius, const sa::color& color, float scale, int thickness)
{
    mapBitmap_.draw_circle(center.x_, center.y_, radius * scale, color, thickness);
}

void MissionMapWindow::DrawObject(const IntVector2& pos, DotType type, bool isSelected, bool isDead, Game::TeamColor teamColor)
{
    if (pos.x_ < 0 || pos.x_ > MAP_WIDTH || pos.y_ < 0 || pos.y_ > MAP_HEIGHT)
        return;
    const sa::color* color = nullptr;
    auto* cache = GetSubsystem<ResourceCache>();
    Image* img = nullptr;

    switch (type)
    {
    case DotType::Self:
        img = cache->GetResource<Image>("Textures/Minimaps/marker_actor.png");
        color = &SELF_COLOR;
        break;
    case DotType::Ally:
        img = cache->GetResource<Image>("Textures/Minimaps/marker_actor.png");
        switch (teamColor)
        {
        case Game::TeamColor::Blue:
            color = &BLUE_TEAM_COLOR;
            break;
        case Game::TeamColor::Red:
            color = &RED_TEAM_COLOR;
            break;
        case Game::TeamColor::Yellow:
            color = &YELLOW_TEAM_COLOR;
            break;
        default:
            color = &ALLY_COLOR;
        }
        break;
    case DotType::Other:
        img = cache->GetResource<Image>("Textures/Minimaps/marker_actor.png");
        switch (teamColor)
        {
        case Game::TeamColor::Blue:
            color = &BLUE_TEAM_COLOR;
            break;
        case Game::TeamColor::Red:
            color = &RED_TEAM_COLOR;
            break;
        case Game::TeamColor::Yellow:
            color = &YELLOW_TEAM_COLOR;
            break;
        default:
            color = &OTHER_COLOR;
        }
        break;
    case DotType::Foe:
    {
        img = cache->GetResource<Image>("Textures/Minimaps/marker_actor.png");
        switch (teamColor)
        {
        case Game::TeamColor::Blue:
            color = &BLUE_TEAM_COLOR;
            break;
        case Game::TeamColor::Red:
            color = &RED_TEAM_COLOR;
            break;
        case Game::TeamColor::Yellow:
            color = &YELLOW_TEAM_COLOR;
            break;
        default:
            color = &FOE_COLOR;
        }
        break;
    }
    case DotType::Waypoint:
        img = cache->GetResource<Image>("Textures/Minimaps/marker_waypoint.png");
        color = &WAYPOINT_COLOR;
        break;
    case DotType::PingPos:
        img = cache->GetResource<Image>("Textures/Minimaps/marker_ping.png");
        color = &PING_COLOR;
        break;
    case DotType::Marker:
        img = cache->GetResource<Image>("Textures/Minimaps/marker_marker.png");
        color = &MARKER_COLOR;
        break;
    case DotType::Portal:
        img = cache->GetResource<Image>("Textures/Minimaps/marker_portal.png");
        break;
    case DotType::AccountChest:
        img = cache->GetResource<Image>("Textures/Minimaps/marker_account_chest.png");
        break;
    case DotType::ResShrine:
        img = cache->GetResource<Image>("Textures/Minimaps/marker_res_shrine.png");
        break;
    }
    if (!img)
        return;

    if (color)
    {
        sa::color baseColor = *color;
        if (isDead)
            baseColor.set_saturation(baseColor.saturation() * 0.2f);
        const sa::bitmap objectBmp = { img->GetWidth(), img->GetHeight(), (int)img->GetComponents(), img->GetData() };
        mapBitmap_.alpha_blend_lerp(pos.x_ - (objectBmp.width() / 2), pos.y_ - (objectBmp.height() / 2),
            objectBmp, baseColor, 0.5f, false);
        if (isSelected)
            DrawCircle(pos, 8, baseColor.filtered<sa::filter::tint>({ 0.7f }), 1.0f);
    }
    else
    {
        const sa::bitmap objectBmp = { img->GetWidth(), img->GetHeight(), (int)img->GetComponents(), img->GetData() };
        mapBitmap_.alpha_blend(pos.x_ - (objectBmp.width() / 2), pos.y_ - (objectBmp.height() / 2),
            objectBmp, false);
    }
}

Player* MissionMapWindow::GetPlayer() const
{
    LevelManager* lm = GetSubsystem<LevelManager>();
    return lm->GetPlayer();
}

void MissionMapWindow::DrawRanges()
{
    DrawCircle({ MAP_WIDTH / 2, MAP_HEIGHT / 2 }, Game::RANGE_AGGRO, AGGRO_RANGE_COLOR);
    DrawCircle({ MAP_WIDTH / 2, MAP_HEIGHT / 2 }, Game::RANGE_CASTING, CASTING_RANGE_COLOR);
}

void MissionMapWindow::DrawObjects()
{
    auto* lm = GetSubsystem<LevelManager>();
    WorldLevel* lvl = lm->GetCurrentLevel<WorldLevel>();
    if (!lvl)
        return;

    if (auto* p = GetPlayer())
    {
        const Vector3& center = p->GetNode()->GetPosition();
        // Draw waypoints at the bottom
        for (const auto& wp : waypoints_)
        {
            const IntVector2 mapPos = WorldToMapPos(center, wp);
            DrawObject(mapPos, DotType::Waypoint, false, false);
        }

        const IntVector2 origin = WorldToMap(center);

        // TODO: Fix this! Heck that's 3 coordinate systems. You shouldn't make a game when you can't to some math :/
        // However, this is just trying to get some texture displayed as map.
        IntVector2 fullSize = {
            int(terrainScaling_.x_ * heightmapTexture_->GetWidth() * (float)SCALE),
            int(terrainScaling_.y_ * heightmapTexture_->GetHeight() * (float)SCALE)
        };
        IntVector2 sourceExtends = {
            int(((float)MAP_WIDTH / (float)SCALE) / 2.0f),
            int(((float)MAP_HEIGHT / (float)SCALE) / 2.0f)
        };
        IntVector2 imageCenter = { (fullSize.x_ / 2 ),
                (fullSize.y_ / 2 ) };

        IntRect imageRect = {
            (origin.x_ - sourceExtends.x_),
            (origin.y_ - sourceExtends.y_),
            (origin.x_ + sourceExtends.x_),
            (origin.y_ + sourceExtends.y_)
        };

//        URHO3D_LOGINFOF("fullSize: %s, sourceExtends: %s, imageRect: %s, origin: %s",
//            fullSize.ToString().CString(), sourceExtends.ToString().CString(),
//            imageRect.ToString().CString(), origin.ToString().CString());

        terrainLayer_->SetImageRect(imageRect);
//        terrainLayer_->SetFullImageRect();

        uint32_t selectedId = p->GetSelectedObjectId();
        lvl->VisitObjects([this, &center, p, selectedId](GameObject& current)
        {
            if (!Is<Actor>(current) || !current.IsPlayingCharacterOrNpc())
                return Iteration::Continue;
            const IntVector2 mapPos = WorldToMapPos(center, current.GetNode()->GetPosition());
            DotType type;
            if (current.GetID() == p->GetID())
                type = DotType::Self;
            else if (current.npcType_ == AB::GameProtocol::NpcType::Portal)
                type = DotType::Portal;
            else if (current.npcType_ == AB::GameProtocol::NpcType::AccountChest)
                type = DotType::AccountChest;
            else if (current.npcType_ == AB::GameProtocol::NpcType::ResurrectionShrine)
                type = DotType::ResShrine;
            else if (p->IsAlly(&To<Actor>(current)))
                type = DotType::Ally;
            else if (p->IsEnemy(&To<Actor>(current)))
                type = DotType::Foe;
            else
                type = DotType::Other;
            const bool selected = selectedId == current.gameId_;
            DrawObject(mapPos, type, selected, To<Actor>(current).IsDead(), current.groupColor_);
            return Iteration::Continue;
        });

        if (haveMarker_)
            DrawObject(WorldToMapPos(center, marker_), DotType::Marker, false, false);

        if (pingTime_ != 0)
        {
            // Show pinged position/target for 2 seconds
            if (sa::time::time_elapsed(pingTime_) < 2000)
            {
                switch (pingType_)
                {
                case PingType::Position:
                    DrawObject(WorldToMapPos(center, pingPos_), DotType::PingPos, false, false);
                    break;
                case PingType::Target:
                {
                    if (auto t = target_.Lock())
                    {
                        const Vector3& targetPos = t->GetNode()->GetPosition();
                        DrawObject(WorldToMapPos(center, targetPos), DotType::PingPos, false, false);
                    }
                    break;
                }
                default:
                    break;
                }
            }
        }
    }
}

void MissionMapWindow::HandleRenderUpdate(StringHash, VariantMap&)
{
    if (!IsVisible())
        return;
    mapBitmap_.clear();
    DrawRanges();
    DrawObjects();
    mapTexture_->SetData(mapImage_, true);
}

void MissionMapWindow::HandleUpdate(StringHash, VariantMap&)
{
    if (!AB::Entities::IsBattle(gameType_))
        return;
    auto* p = GetPlayer();
    if (!p)
        return;

    const auto& pos = p->GetNode()->GetPosition();
    if (waypoints_.Empty() ||
        pos.DistanceToPoint(waypoints_.Back()) > 5.0f)
        waypoints_.Push(pos);
}

void MissionMapWindow::HandleResized(StringHash, VariantMap&)
{
    FitTexture();
}

void MissionMapWindow::HandlePositionPinged(StringHash, VariantMap& eventData)
{
    using namespace Events::PositionPinged;
    pingType_ = PingType::Position;
    pingTime_ = sa::time::tick();
    pingerId_ = eventData[P_OBJECTID].GetUInt();
    pingPos_ = eventData[P_POSITION].GetVector3();
    auto* audio = GetSubsystem<AudioManager>();
    audio->PlaySound("Sounds/FX/Ping.wav", SOUND_EFFECT);
}

void MissionMapWindow::HandleMouseDown(StringHash, VariantMap& eventData)
{
    using namespace MouseButtonDown;
    (void)eventData;
}

void MissionMapWindow::HandleMouseUp(StringHash, VariantMap& eventData)
{
    using namespace MouseButtonUp;
    if (eventData[P_BUTTON].GetUInt() != MOUSEB_LEFT)
        return;

    auto* input = GetSubsystem<Input>();
    UI* ui = GetSubsystem<UI>();
    auto* elem = ui->GetElementAt(input->GetMousePosition(), false);
    if (elem == nullptr || elem != objectLayer_.Get())
        return;

    if (!terrainLayer_->IsInside(input->GetMousePosition(), true))
        return;

    if (auto* p = GetPlayer())
    {
        IntVector2 relativePos = input->GetMousePosition() - terrainLayer_->GetScreenPosition();
        relativePos.y_ = terrainLayer_->GetHeight() - relativePos.y_;
        Vector3 worldPos = MapToWorldPos(p->GetNode()->GetPosition(), relativePos);
        auto* client = GetSubsystem<FwClient>();
        client->PingPosition(worldPos);
    }
}

void MissionMapWindow::HandleTargetPinged(StringHash, VariantMap& eventData)
{
    using namespace Events::ObjectPingTarget;

    uint32_t targetId = eventData[P_TARGETID].GetUInt();
    auto* lm = GetSubsystem<LevelManager>();
    target_ = lm->GetObject(targetId);
    if (!target_)
        return;

    pingType_ = PingType::Target;
    pingerId_ = eventData[P_OBJECTID].GetUInt();
    pingTime_ = sa::time::tick();
    auto* audio = GetSubsystem<AudioManager>();
    audio->PlaySound("Sounds/FX/Ping.wav", SOUND_EFFECT);
}

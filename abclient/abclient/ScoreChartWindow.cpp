/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "ScoreChartWindow.h"
#include "Shortcuts.h"
#include "LevelManager.h"
#include "WorldLevel.h"
#include <map>

void ScoreChartWindow::RegisterObject(Context* context)
{
    context->RegisterFactory<ScoreChartWindow>();
    URHO3D_COPY_BASE_ATTRIBUTES(Window);
}

ScoreChartWindow::ScoreChartWindow(Context* context) :
    Window(context)
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    XMLFile* file = cache->GetResource<XMLFile>("UI/ScoreChartWindow.xml");
    LoadXML(file->GetRoot(), nullptr);
    SetName(ScoreChartWindow::GetTypeNameStatic());

    Shortcuts* scs = GetSubsystem<Shortcuts>();
    Text* caption = GetChildStaticCast<Text>("CaptionText", true);
    caption->SetText(scs->GetCaption(Events::E_SC_TOGGLESCORECHARTWINDOW, "Score Chart", true));

    auto* metricsDropdown = GetChildStaticCast<DropDownList>("MetricsDropDown", true);
    metricsDropdown->SetResizePopup(true);
    {
        Text* item = new Text(context_);
        item->SetText("Health");
        item->SetVar("Int Value", static_cast<unsigned>(Metrics::Health));
        item->SetStyle("DropDownItemEnumText");
        metricsDropdown->AddItem(item);
    }
    {
        Text* item = new Text(context_);
        item->SetText("Morale");
        item->SetVar("Int Value", static_cast<unsigned>(Metrics::Morale));
        item->SetStyle("DropDownItemEnumText");
        metricsDropdown->AddItem(item);
    }
    SubscribeToEvent(metricsDropdown, E_ITEMSELECTED, [this](StringHash, VariantMap& eventData)
    {
        using namespace ItemSelected;
        unsigned sel = eventData[P_SELECTION].GetUInt();
        metrics_ = static_cast<Metrics>(sel);
    });

    auto* container = GetChildStaticCast<BorderImage>("Container", true);
    container->SetOpacity(1.0f);
    container->SetUseDerivedOpacity(false);

    chartTexture_ = MakeShared<Texture2D>(context_);
    chartTexture_->SetSize(CHART_WIDTH, CHART_HEIGHT, Graphics::GetRGBAFormat(), TEXTURE_DYNAMIC);
    chartTexture_->SetNumLevels(1);
    chartTexture_->SetMipsToSkip(QUALITY_LOW, 0);
    chartImage_ = MakeShared<Image>(context_);
    chartImage_->SetSize(CHART_WIDTH, CHART_HEIGHT, 4);
    chartBitmap_.set_bitmap(CHART_WIDTH, CHART_HEIGHT, 4, chartImage_->GetData());
    chartTexture_->SetData(chartImage_, false);

    container->SetTexture(chartTexture_);
    container->SetFullImageRect();

    SetStyleAuto();
    UpdateLayout();

    Clear();

    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(ScoreChartWindow, HandleUpdate));
    SubscribeToEvent(E_RENDERUPDATE, URHO3D_HANDLER(ScoreChartWindow, HandleRenderUpdate));
    SubscribeToEvent(Events::E_LEVELREADY, URHO3D_HANDLER(ScoreChartWindow, HandleLevelReady));
}

ScoreChartWindow::~ScoreChartWindow()
{ }

void ScoreChartWindow::HandleLevelReady(StringHash, VariantMap&)
{
    Clear();
}

void ScoreChartWindow::HandleRenderUpdate(StringHash, VariantMap&)
{
    if (!IsVisible())
        return;

    static constexpr sa::color background{ 0, 0, 0, 190 };
    chartBitmap_.clear(background);
    DrawChart();
    chartTexture_->SetData(chartImage_, true);
}

void ScoreChartWindow::HandleUpdate(StringHash, VariantMap& eventData)
{
    using namespace Update;
    float timeStep = eventData[P_TIMESTEP].GetFloat();
    time_ += timeStep;

    collectTime_ += timeStep;
    if (collectTime_ >= 1.0f)
    {
        if (samples_.Size() > SAMPLES_TO_KEEP)
            // Delete some more so it doesn't have to reallocated/move stuff every update
            samples_.Erase(0, 100);
        CollectData();
        collectTime_ = 0.0f;
    }
}

void ScoreChartWindow::DrawPoint(const IntVector2& center, int size, const sa::color& color)
{
    chartBitmap_.draw_point(center.x_, center.y_, size, color);
}

void ScoreChartWindow::DrawLine(const IntVector2& p1, const IntVector2& p2, int size, const sa::color& color)
{
    if (p1.x_ == 0 && p1.y_ == 0)
        return chartBitmap_.draw_point(p2.x_, p2.y_, size, color);

    chartBitmap_.draw_line(p1.x_, p1.y_, p2.x_, p2.y_, size, color);
}

int ScoreChartWindow::GetValue(const ScoreChartWindow::Sample& sample) const
{
    switch (metrics_)
    {
    case Metrics::Health:
        if (maxHealth_ == 0)
            return 0;
        return CHART_HEIGHT - (int)(((float)CHART_HEIGHT * 0.5f) * ((float)sample.health / (float)maxHealth_));
    case Metrics::Morale:
        if (sample.actors == 0)
            return 0;
        return CHART_HEIGHT - (int)(((float)CHART_HEIGHT * 0.5f) + ((float)sample.morale));
    default:
        return 0;
    }
}

void ScoreChartWindow::DrawChart()
{
    static constexpr sa::color RED_TEAM_COLOR{ 255, 0, 0 };
    static constexpr sa::color BLUE_TEAM_COLOR{ 0, 0, 255 };
    static constexpr sa::color YELLOW_TEAM_COLOR{ 255, 255, 0 };
    std::map<Game::TeamColor, IntVector2> fromPoint;

    for (const auto& sample : samples_)
    {
        const int x = CHART_WIDTH - (int)(time_ - sample->time);
        if (x < 0)
            continue;

        const int y = GetValue(*sample);
        switch (sample->color)
        {
        case Game::TeamColor::Blue:
            DrawLine(fromPoint[sample->color], { x, y }, 2, BLUE_TEAM_COLOR);
            break;
        case Game::TeamColor::Red:
            DrawLine(fromPoint[sample->color], { x, y }, 2, RED_TEAM_COLOR);
            break;
        case Game::TeamColor::Yellow:
            DrawLine(fromPoint[sample->color], { x, y }, 2, YELLOW_TEAM_COLOR);
            break;
        default:
//            DrawPoint({ x, y }, 2, RED_TEAM_COLOR);
            break;
        }
        fromPoint[sample->color] = { x, y };
    }
}

void ScoreChartWindow::CollectData()
{
    auto* lm = GetSubsystem<LevelManager>();
    auto* level = lm->GetCurrentLevel<WorldLevel>();
    if (!level)
        return;

    std::map<uint32_t, Sample> teams;
    level->VisitObjects([this, &teams](const GameObject& current) -> Iteration
    {
        if (!current.IsPlayingCharacterOrNpc())
            return Iteration::Continue;
        const auto& actor = To<Actor>(current);
        if (actor.groupId_ == 0)
            return Iteration::Continue;

        if (!teams.contains(actor.groupId_))
        {
            Sample sample;
            sample.time = time_;
            sample.groupId = actor.groupId_;
            sample.color = actor.groupColor_;
            sample.actors = 1;
            sample.morale = actor.stats_.morale;
            sample.health = actor.stats_.health;
            sample.maxHealth = actor.stats_.maxHealth;
            teams.emplace(actor.groupId_, std::move(sample));
            return Iteration::Continue;
        }
        auto& team = teams[actor.groupId_];
        ++team.actors;
        team.health += actor.stats_.health;
        team.maxHealth += actor.stats_.maxHealth;
        team.morale += actor.stats_.morale;

        return Iteration::Continue;
    });

    maxHealth_ = 0;
    for (auto& team : teams)
    {
        maxHealth_ = Max(maxHealth_, team.second.maxHealth);
        samples_.Push(MakeUnique<Sample>(team.second));
    }
}

void ScoreChartWindow::Clear()
{
    time_ = 0.0f;
    samples_.Clear();
}

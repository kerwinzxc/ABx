/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <Urho3DAll.h>
#include <sa/bitmap.h>
#include "Actor.h"

class ScoreChartWindow : public Window
{
    URHO3D_OBJECT(ScoreChartWindow, Window)
private:
    static constexpr unsigned CHART_WIDTH = 680;
    static constexpr unsigned CHART_HEIGHT = 500;
    // 1800 samples = 1/2 hour for max. 3 teams
    static constexpr unsigned SAMPLES_TO_KEEP = 1800 * 3;
    struct Sample
    {
        Sample() {}
        Sample(const Sample& other) :
            time(other.time),
            groupId(other.groupId),
            color(other.color),
            actors(other.actors),
            morale(other.morale),
            health(other.health),
            maxHealth(other.maxHealth)
        {}
        float time{ 0.0f };
        uint32_t groupId{ 0 };
        Game::TeamColor color{ Game::TeamColor::Default };
        unsigned actors{ 0 };
        int morale{ 0 };
        int health{ 0 };
        int maxHealth{ 0 };
    };
    enum class Metrics
    {
        Health = 0,
        Morale
    };
    Metrics metrics_{ Metrics::Health };
    SharedPtr<Image> chartImage_;
    sa::bitmap chartBitmap_;
    SharedPtr<Texture2D> chartTexture_;
    float collectTime_{ 0.0f };
    float time_{ 0.0f };
    int maxHealth_{ 0 };
    Vector<UniquePtr<Sample>> samples_;
    void HandleRenderUpdate(StringHash eventType, VariantMap& eventData);
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleLevelReady(StringHash eventType, VariantMap& eventData);
    void DrawChart();
    void CollectData();
    void DrawPoint(const IntVector2& center, int size, const sa::color& color);
    void DrawLine(const IntVector2& p1, const IntVector2& p2, int size, const sa::color& color);
    int GetValue(const Sample& sample) const;
public:
    static void RegisterObject(Context* context);

    ScoreChartWindow(Context* context);
    ~ScoreChartWindow() override;

    void Clear();
};


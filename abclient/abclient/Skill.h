/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <AB/Entities/Skill.h>

struct Skill
{
    std::string uuid;
    uint32_t index{ 0 };
    uint32_t access{ AB::Entities::SkillAccessNone };
    std::string name;
    std::string attributeUuid;
    std::string professionUuid;
    AB::Entities::SkillType type{ AB::Entities::SkillTypeSkill };
    bool isElite{ false };
    std::string description;
    std::string shortDescription;
    std::string icon;
    std::string soundEffect;
    std::string particleEffect;
    int32_t activation{ 0 };
    int32_t recharge{ 0 };
    int32_t costEnergy{ 0 };
    int32_t costEnergyRegen{ 0 };
    int32_t costAdrenaline{ 0 };
    int32_t costOvercast{ 0 };
    int32_t costHp{ 0 };
};

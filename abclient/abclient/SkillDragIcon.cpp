#include "SkillDragIcon.h"

void SkillDragIcon::RegisterObject(Context* context)
{
    context->RegisterFactory<SkillDragIcon>();
    URHO3D_COPY_BASE_ATTRIBUTES(Window);
}

SkillDragIcon::SkillDragIcon(Context* context) :
    Window(context)
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    Texture2D* tex = cache->GetResource<Texture2D>("Textures/UI.png");
    SetLayout(LM_HORIZONTAL);
    SetLayoutBorder(IntRect(4, 4, 4, 4));
    SetTexture(tex);
    SetImageRect(IntRect(48, 0, 64, 16));
    SetBorder(IntRect(4, 4, 4, 4));
    SetMinSize(60, 60);
    SetMaxSize(60, 60);
}

SkillDragIcon::~SkillDragIcon()
{ }

void SkillDragIcon::SetSkill(const UIElement& elem, Texture* tex)
{
    BorderImage* icon = CreateChild<BorderImage>();
    icon->SetTexture(tex);
    SetPosition(elem.GetPosition());
    SetVar("SkillIndex", elem.GetVar("SkillIndex"));
    SetVar("SkillPos", elem.GetVar("SkillPos"));
}

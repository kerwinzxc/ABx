/**
 * Copyright 2020 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "SkillTooltip.h"
#include "TemplateEvaluator.h"
#include "Conversions.h"

void SkillTooltip::RegisterObject(Context* context)
{
    context->RegisterFactory<SkillTooltip>();
    URHO3D_COPY_BASE_ATTRIBUTES(ToolTip);
}

SkillTooltip::SkillTooltip(Context* context) :
    ToolTip(context)
{
    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    auto* cache = GetSubsystem<ResourceCache>();
    SetVisible(false);
    window_ = CreateChild<Window>("SkillTooltipWindow");

    XMLFile* xml = cache->GetResource<XMLFile>("UI/SkillsTooltip.xml");
    window_->LoadXML(xml->GetRoot());

    skillName_ = window_->GetChildStaticCast<Text>("SkillName", true);
    UIElement* skillCostContainer = window_->GetChildStaticCast<UIElement>("SkillCost", true);
    skillCost_ = skillCostContainer->CreateChild<SkillCostElement>();
    skillDescription_ = window_->GetChildStaticCast<Text>("SkillDescription", true);
}

SkillTooltip::~SkillTooltip()
{ }

void SkillTooltip::HandleSetAttribValue(StringHash, VariantMap& eventData)
{
    if (!skill_)
        return;
    auto actor = actor_.Lock();
    if (!actor)
        return;
    using namespace Events::SetAttributeValue;
    if (eventData[P_OBJECTID].GetUInt() != actor->gameId_)
        return;

    TemplateEvaluator templEval(*actor);
    skillDescription_->SetText(ToUrhoString(templEval.Evaluate(skill_->description)));
}

void SkillTooltip::SetSkill(const Skill* skill, Actor* actor)
{
    if (skill_ == skill && actor_.Get() == actor)
        return;

    skill_ = skill;
    actor_ = actor;
    if (actor && skill)
        SubscribeToEvent(Events::E_SET_ATTRIBUTEVALUE, URHO3D_HANDLER(SkillTooltip, HandleSetAttribValue));
    else
        UnsubscribeFromEvent(Events::E_SET_ATTRIBUTEVALUE);

    if (skill_)
    {
        skillCost_->SetSkill(*skill_);
        skillName_->SetText(ToUrhoString(skill->name));
        if (actor)
        {
            TemplateEvaluator templEval(*actor);
            skillDescription_->SetText(ToUrhoString(templEval.Evaluate(skill->description)));
        }
        else
            skillDescription_->SetText(ToUrhoString(skill->shortDescription));
        // This UI layouting to so totally broken, it's not even funny anymore...
        UpdateLayout();
        SetPosition({ 0, -(window_->GetHeight() + 15) });
        SetEnabled(true);
    }
    else
    {
        skillName_->SetText("");
        skillDescription_->SetText("");
        skillCost_->RemoveAllChildren();
        SetEnabled(false);
    }
}

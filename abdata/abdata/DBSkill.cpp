/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "DBSkill.h"
#include <sa/TemplateParser.h>

namespace DB {

static std::string PlaceholderCallback(Database* db, const AB::Entities::Skill& skill, const sa::templ::Token& token)
{
    switch (token.type)
    {
    case sa::templ::Token::Type::Variable:
        if (token.value == "uuid")
            return db->EscapeString(skill.uuid);
        if (token.value == "idx")
            return std::to_string(skill.index);

        LOG_WARNING << "Unhandled placeholder " << token.value << std::endl;
        return "";
    default:
        return token.value;
    }
}

bool DBSkill::Create(AB::Entities::Skill& skill)
{
    // Do nothing
    if (Utils::Uuid::IsEmpty(skill.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

bool DBSkill::Load(AB::Entities::Skill& skill)
{
    Database* db = GetSubsystem<Database>();

    sa::templ::Parser parser;
    sa::templ::Tokens tokens = parser.Parse("SELECT * FROM game_skills WHERE ");
    if (!Utils::Uuid::IsEmpty(skill.uuid))
        parser.Append("uuid = ${uuid}", tokens);
    else
        // 0 is a valid index, it is the None skill
        parser.Append("idx = ${idx}", tokens);
    const std::string query = tokens.ToString(std::bind(&PlaceholderCallback, db, skill, std::placeholders::_1));

    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query);
    if (!result)
        return false;

    skill.uuid = result->GetString("uuid");
    skill.index = result->GetUInt("idx");
    skill.script = result->GetString("script");
    skill.access = result->GetUInt("access");

    return true;
}

bool DBSkill::Save(const AB::Entities::Skill& skill)
{
    // Do nothing
    if (Utils::Uuid::IsEmpty(skill.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

bool DBSkill::Delete(const AB::Entities::Skill& skill)
{
    // Do nothing
    if (Utils::Uuid::IsEmpty(skill.uuid))
    {
        LOG_ERROR << "UUID is empty" << std::endl;
        return false;
    }
    return true;
}

bool DBSkill::Exists(const AB::Entities::Skill& skill)
{
    Database* db = GetSubsystem<Database>();

    sa::templ::Parser parser;
    sa::templ::Tokens tokens = parser.Parse("SELECT COUNT(*) AS count FROM game_skills WHERE ");
    if (!Utils::Uuid::IsEmpty(skill.uuid))
        parser.Append("uuid = ${uuid}", tokens);
    else
        // 0 is a valid index, it is the None skill
        parser.Append("idx = ${idx}", tokens);
    const std::string query = tokens.ToString(std::bind(&PlaceholderCallback, db, skill, std::placeholders::_1));

    std::shared_ptr<DB::DBResult> result = db->StoreQuery(query);
    if (!result)
        return false;
    return result->GetUInt("count") != 0;
}

}

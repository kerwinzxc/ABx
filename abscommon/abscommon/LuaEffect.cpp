/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "LuaEffect.h"
#include "StringUtils.h"
#include "FileUtils.h"
#include "UuidUtils.h"
#include <sa/StringTempl.h>

extern std::string gDataDir;

namespace IO {

static void InitState(kaguya::State& state)
{
    state["include"] = kaguya::function([&state](const std::string& file)
    {
        std::string scriptFile = Utils::ConcatPath(gDataDir, file);
        if (!Utils::FileExists(scriptFile))
            return;

        // Make something like an include guard
        std::string ident(file);
        sa::MakeIdent(ident);
        ident = "__included_" + ident + "__";
        if (state[ident].type() == LUA_TBOOLEAN)
            return;
        if (!state.dofile(scriptFile.c_str()))
        {
            std::cerr << lua_tostring(state.state(), -1) << std::endl;
            return;
        }
        state[ident] = true;
    });
}

LuaEffect::LuaEffect()
{
    InitState(state_);
}

bool LuaEffect::Execute(const std::string& script)
{
    if (!state_.dofile(script.c_str()))
    {
        std::cerr << lua_tostring(state_.state(), -1) << std::endl;
        return false;
    }
    return true;
}

std::string LuaEffect::GetUuid()
{
    if (state_["uuid"].type() == LUA_TSTRING)
        return state_["uuid"];
    return Utils::Uuid::New();
}

int32_t LuaEffect::GetIndex()
{
    if (state_["index"].type() == LUA_TNUMBER)
        return state_["index"];
    return 0;
}

std::string LuaEffect::GetName()
{
    if (state_["name"].type() == LUA_TSTRING)
        return state_["name"];
    return "";
}

std::string LuaEffect::GetIcon()
{
    if (state_["icon"].type() == LUA_TSTRING)
        return state_["icon"];
    return "";
}

std::string LuaEffect::GetSoundEffect()
{
    if (state_["soundEffect"].type() == LUA_TSTRING)
        return state_["soundEffect"];
    return "";
}

std::string LuaEffect::GetParticleEffect()
{
    if (state_["particleEffect"].type() == LUA_TSTRING)
        return state_["particleEffect"];
    return "";
}

AB::Entities::EffectCategory LuaEffect::GetCategory()
{
    if (state_["category"].type() == LUA_TNUMBER)
        return static_cast<AB::Entities::EffectCategory>(state_["category"]);
    return AB::Entities::EffectNone;
}

}

/**
 * Copyright 2020-2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "LuaSkill.h"
#include "StringUtils.h"
#include "FileUtils.h"
#include "UuidUtils.h"
#include <sa/StringTempl.h>
#include <iostream>

extern std::string gDataDir;

namespace IO {

static void InitState(kaguya::State& state)
{
    state["include"] = kaguya::function([&state](const std::string& file)
    {
        std::string scriptFile = Utils::ConcatPath(gDataDir, file);
        if (!Utils::FileExists(scriptFile))
            return;

        // Make something like an include guard
        std::string ident(file);
        sa::MakeIdent(ident);
        ident = "__included_" + ident + "__";
        if (state[ident].type() == LUA_TBOOLEAN)
            return;
        if (!state.dofile(scriptFile.c_str()))
        {
            std::cerr << lua_tostring(state.state(), -1) << std::endl;
            return;
        }
        state[ident] = true;
    });
}

LuaSkill::LuaSkill()
{
    InitState(state_);
}

bool LuaSkill::Execute(const std::string& script)
{
    if (!state_.dofile(script.c_str()))
    {
        std::cerr << lua_tostring(state_.state(), -1) << std::endl;
        return false;
    }
    return true;
}

std::string LuaSkill::GetUuid()
{
    if (state_["uuid"].type() == LUA_TSTRING)
        return state_["uuid"];
    return Utils::Uuid::New();
}

int32_t LuaSkill::GetIndex()
{
    if (state_["index"].type() == LUA_TNUMBER)
        return state_["index"];
    return 0;
}

std::string LuaSkill::GetName()
{
    if (state_["name"].type() == LUA_TSTRING)
        return state_["name"];
    return "";
}

uint32_t LuaSkill::GetAttribute()
{
    if (state_["attribute"].type() == LUA_TNUMBER)
        return state_["attribute"];
    return 99;
}

AB::Entities::SkillType LuaSkill::GetSkillType()
{
    if (state_["skillType"].type() == LUA_TNUMBER)
        return static_cast<AB::Entities::SkillType>(state_["skillType"]);
    return AB::Entities::SkillTypeSkill;
}

bool LuaSkill::IsElite()
{
    if (state_["isElite"].type() == LUA_TBOOLEAN)
        return state_["isElite"];
    return false;
}

std::string LuaSkill::GetDescription()
{
    if (state_["description"].type() == LUA_TSTRING)
        return state_["description"];
    return "";
}

std::string LuaSkill::GetShortDescription()
{
    if (state_["shortDescription"].type() == LUA_TSTRING)
        return state_["shortDescription"];
    return "";
}

std::string LuaSkill::GetIcon()
{
    if (state_["icon"].type() == LUA_TSTRING)
        return state_["icon"];
    return "";
}

AB::Entities::ProfessionIndex LuaSkill::GetProfession()
{
    if (state_["profession"].type() == LUA_TNUMBER)
        return static_cast<AB::Entities::ProfessionIndex>(state_["profession"]);
    return AB::Entities::ProfessionIndexNone;
}

std::string LuaSkill::GetSoundEffect()
{
    if (state_["soundEffect"].type() == LUA_TSTRING)
        return state_["soundEffect"];
    return "";
}

std::string LuaSkill::GetParticleEffect()
{
    if (state_["particleEffect"].type() == LUA_TSTRING)
        return state_["particleEffect"];
    return "";
}

AB::Entities::SkillAccess LuaSkill::GetAccess()
{
    if (state_["access"].type() == LUA_TNUMBER)
        return static_cast<AB::Entities::SkillAccess>(state_["access"]);
    return AB::Entities::SkillAccessPlayer;
}

int32_t LuaSkill::GetEnergy()
{
    return state_["costEnergy"];
}

int32_t LuaSkill::GetEnergyRegen()
{
    if (state_["costEnergyRegen"].type() == LUA_TNUMBER)
        return state_["costEnergyRegen"];
    return 0;
}

int32_t LuaSkill::GetAdrenaline()
{
    return state_["costAdrenaline"];
}

int32_t LuaSkill::GetActivation()
{
    return state_["activation"];
}

int32_t LuaSkill::GetOvercast()
{
    return state_["overcast"];
}

int32_t LuaSkill::GetHp()
{
    if (state_["hp"].type() == LUA_TNUMBER)
        return state_["hp"];
    return 0;
}

int32_t LuaSkill::GetRecharge()
{
    return state_["recharge"];
}

}

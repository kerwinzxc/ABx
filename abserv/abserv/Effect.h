/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <memory>
#include <kaguya/kaguya.hpp>
#include <sa/PropStream.h>
#include <AB/Entities/Effect.h>
#include <AB/Entities/Skill.h>
#include <abshared/Damage.h>
#include <abshared/Attributes.h>
#include <sa/Noncopyable.h>
#include <sa/Bits.h>
#include <eastl.hpp>

namespace Game {

class Actor;
class Skill;
class Item;

enum EffectAttr : uint8_t
{
    EffectAttrId = 1,
    EffectAttrTicks,

    // For serialization
    EffectAttrEnd = 254
};

class Effect
{
    NON_COPYABLE(Effect)
private:
    enum Function : uint64_t
    {
        FunctionNone         = 0,
        FunctionUpdate       = 1ull,
        FunctionGetSkillCost = 1ull << 1,
        FunctionGetDamage    = 1ull << 2,
        FunctionGetAttackSpeed = 1ull << 3,
        FunctionGetAttackDamageType = 1ull << 4,
        FunctionGetAttackDamage = 1ull << 5,
        FunctionOnAttack = 1ull << 6,
        FunctionOnGettingAttacked = 1ull << 7,
        FunctionOnCanUseSkill = 1ull << 8,
        FunctionOnGettingSkillTarget = 1ull << 9,
        FunctionOnAttacked = 1ull << 10,
        FunctionOnInterruptingAttack = 1ull << 11,
        FunctionOnInterruptingSkill = 1ull << 12,
        FunctionOnKnockingDown = 1ull << 13,
        FunctionOnHealing = 1ull << 14,
        FunctionOnGetCriticalHit = 1ull << 15,
        FunctionGetArmor = 1ull << 16,
        FunctionGetArmorPenetration = 1ull << 17,
        FunctionGetAttributeRank = 1ull << 18,
        FunctionGetResources = 1ull << 19,
        FunctionGetSkillRecharge = 1ull << 20,
        FunctionOnRemoved = 1ull << 21,
        FunctionOnKnockedDown = 1ull << 22,
        FunctionOnStart = 1ull << 23,
        FunctionOnEnd = 1ull << 24,
        FunctionGetDuration = 1ull << 25,
        FunctionGetSkillActivation = 1ull << 26,
        FunctionOnEndUseSkill = 1ull << 27,
        FunctionOnAddEffect = 1ull << 28,
        FunctionOnSkillTargeted = 1ull << 29,
        FunctionOnKnockingDownTarget = 1ull << 30,
        FunctionOnInterruptingTarget = 1ull << 31,
        FunctionSourceArmorPenetration = 1ull << 32,
        FunctionOnStartUseSkill = 1ull << 33,
    };
    int64_t startTime_;
    int64_t endTime_;
    /// Duration
    uint32_t ticks_;
    kaguya::State luaState_;
    ea::weak_ptr<Actor> target_;
    ea::weak_ptr<Actor> source_;
    bool persistent_{ false };
    uint64_t functions_{ FunctionNone };
    /// Internal effects are not visible to the player.
    bool internal_{ false };
    bool UnserializeProp(EffectAttr attr, sa::PropReadStream& stream);
    void InitializeLua();
    bool HaveFunction(Function func) const
    {
        return sa::bits::is_set(functions_, func);
    }
    Actor* _LuaGetTarget();
    Actor* _LuaGetSource();
    int _LuaGetCategory() const;
public:
    static void RegisterLua(kaguya::State& state);

    Effect() = delete;
    explicit Effect(const AB::Entities::Effect& effect) :
        startTime_(0),
        endTime_(0),
        ticks_(0),
        data_(effect),
        ended_(false),
        cancelled_(false)
    {
        InitializeLua();
    }
    ~Effect() = default;

    /// Gets saved to the DB when player logs out, e.g. Dishonored.
    bool IsPersistent() const
    {
        return persistent_;
    }
    bool IsInternal() const { return internal_; }
    uint32_t GetIndex() const { return effectIndex_; }

    bool LoadScript(const std::string& fileName);
    void Update(uint32_t timeElapsed);
    bool Start(ea::shared_ptr<Actor> source, ea::shared_ptr<Actor> target, uint32_t time);
    /// Remove Effect before it ends
    void Remove();
    /// Get real cost of a skill
    /// \param skill The Skill
    /// \param activation Activation time
    /// \param energy Energy cost
    /// \param adrenaline Adrenaline cost
    /// \param overcast Causes overcast
    /// \param hp HP scarifies in percent of max health
    void GetSkillCost(Skill* skill,
        int32_t& activation, int32_t& energy, int32_t& adrenaline, int32_t& overcast, int32_t& hp);
    void GetSkillRecharge(Skill* skill, uint32_t& recharge);
    void GetSkillActivation(Skill* skill, uint32_t& activation);
    /// Get real damage. It may be in-/decreased by some effects on the *Target*. This is called when the damage is applied to the target.
    void GetDamage(DamageType type, int32_t& value, bool& critical);
    void GetAttackSpeed(Item* weapon, uint32_t& value);
    void GetAttackDamageType(DamageType& type);
    void GetArmor(DamageType type, int& value);
    void GetArmorPenetration(DamageType damageType, float& value);
    void GetSourceArmorPenetration(Actor* target, DamageType damageType, float& value);
    void GetAttributeRank(Attribute index, int32_t& value);
    /// Attack damage may be in-/decreased by effects on the *Source*. This is called when the source starts attacking.
    void GetAttackDamage(int32_t& value);
    void GetRecources(int& maxHealth, int& maxEnergy);
    /// Some effect may make the attacker unable to attack. The target is being attacked.
    void OnAttack(Actor* source, Actor* target, bool& value);
    void OnAttacked(Actor* source, Actor* target, DamageType type, int32_t damage, bool& success);
    /// Checks whether the owner can be attacked
    void OnGettingAttacked(Actor* source, Actor* target, bool& value);
    void OnCanUseSkill(Actor* source, Actor* target, Skill* skill, bool& value);
    void OnGettingSkillTarget(Actor* source, Actor* target, Skill* skill, bool& value);
    void OnSkillTargeted(Actor* source, Actor* target, Skill* skill);
    void OnInterruptingAttack(bool& value);
    void OnInterruptingSkill(Actor* source, Actor* target, AB::Entities::SkillType type, Skill* skill, bool& value);
    void OnKnockingDown(Actor* source, Actor* target, uint32_t time, bool& value);
    void OnKnockedDown(Actor* target, uint32_t time);
    void OnGetCriticalHit(Actor* source, Actor* target, bool& value);
    void OnAddEffect(Actor* source, Actor* target, Effect* effect, bool& value);
    /// Targets gets healed by source
    void OnHealing(Actor* source, Actor* target, int& value);
    void OnStartUseSkill(Actor* source, Actor* target, Skill* skill);
    void OnEndUseSkill(Actor* source, Actor* target, Skill* skill);
    void OnKnockingDownTarget(Actor* source, Actor* target, uint32_t time, bool& success);
    void OnInterruptingTarget(Actor* source, Actor* target, bool& success);

    bool Serialize(sa::PropWriteStream& stream);
    bool Unserialize(sa::PropReadStream& stream);

    uint32_t effectIndex_{ 0 };
    AB::Entities::EffectCategory category_{ AB::Entities::EffectNone };
    AB::Entities::Effect data_;

    bool ended_;
    bool cancelled_;

    int64_t GetStartTime() const { return startTime_; }
    int64_t GetEndTime() const { return endTime_; }
    // Get base time
    uint32_t GetTicks() const { return ticks_; }
    uint32_t GetRemainingTime() const;
};

// Effects are fist-in-last-out
typedef ea::vector<ea::shared_ptr<Effect>> EffectList;

}


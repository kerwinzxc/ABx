/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <AB/ProtocolCodes.h>
#include <sa/Events.h>
#include <sa/StringHash.h>

namespace Net {
class NetworkMessage;
}

namespace AB::Entities {
enum SkillType : uint64_t;
}

namespace Game {

enum class DamageType;
class GameObject;
class Actor;
class Npc;
class Player;
class AreaOfEffect;
class Projectile;
class Skill;
class Effect;

// Events sent by a GameObject
inline constexpr sa::event_t OnClickedEvent = sa::StringHash("OnClicked");
inline constexpr sa::event_t OnCollideEvent = sa::StringHash("OnCollide");
inline constexpr sa::event_t OnLeftAreaEvent = sa::StringHash("OnLeftArea");
inline constexpr sa::event_t OnSelectedEvent = sa::StringHash("OnSelected");
inline constexpr sa::event_t OnTriggerEvent = sa::StringHash("OnTrigger");
inline constexpr sa::event_t OnStateChangeEvent = sa::StringHash("OnStateChange");
inline constexpr sa::event_t OnInteractEvent = sa::StringHash("OnInteract");
inline constexpr sa::event_t OnCancelAllEvent = sa::StringHash("OnCancelAll");

// Actor
inline constexpr sa::event_t OnArrivedEvent = sa::StringHash("OnArrived");
inline constexpr sa::event_t OnInterruptedAttackEvent = sa::StringHash("OnInterruptedAttack");
inline constexpr sa::event_t OnInterruptedSkillEvent = sa::StringHash("OnInterruptedSkill");
inline constexpr sa::event_t OnKnockedDownEvent = sa::StringHash("OnKnockedDown");
inline constexpr sa::event_t OnHealedEvent = sa::StringHash("OnHealed");
// Arguments: Actor* (who died), Actor* (who killed the poor victim)
inline constexpr sa::event_t OnDiedEvent = sa::StringHash("OnDied");
inline constexpr sa::event_t OnResurrectedEvent = sa::StringHash("OnResurrected");
inline constexpr sa::event_t OnPingObjectEvent = sa::StringHash("OnPingObject");
inline constexpr sa::event_t OnInventoryFullEvent = sa::StringHash("OnInventoryFull");
inline constexpr sa::event_t OnChestFullEvent = sa::StringHash("OnChestFull");
// We are attacking a target
inline constexpr sa::event_t OnSourceAttackEvent = sa::StringHash("OnSourceAttack");
// We attacked successfully the target
inline constexpr sa::event_t OnSourceAttackSuccessEvent = sa::StringHash("OnSourceAttackSuccess");
// Attack circle was finished on us
inline constexpr sa::event_t OnTargetAttackedEvent = sa::StringHash("OnTargetAttacked");
// We are the target of an attack
inline constexpr sa::event_t OnTargetAttackEvent = sa::StringHash("OnTargetAttack");
inline constexpr sa::event_t OnCanUseSkillEvent = sa::StringHash("OnCanUseSkill");
inline constexpr sa::event_t OnGettingSkillTargetEvent = sa::StringHash("OnGettingSkillTarget");
inline constexpr sa::event_t OnSkillTargetedEvent = sa::StringHash("OnSkillTargeted");
inline constexpr sa::event_t OnEndUseSkillEvent = sa::StringHash("OnEndUseSkill");
inline constexpr sa::event_t OnStartUseSkillEvent = sa::StringHash("OnStartUseSkill");
inline constexpr sa::event_t OnGetCriticalHitEvent = sa::StringHash("OnGetCriticalHit");
inline constexpr sa::event_t OnHandleCommandEvent = sa::StringHash("OnHandleCommand");
inline constexpr sa::event_t OnInterruptingAttackEvent = sa::StringHash("OnInterruptingAttack");
inline constexpr sa::event_t OnInterruptingSkillEvent = sa::StringHash("OnInterruptingSkill");
inline constexpr sa::event_t OnKnockingDownEvent = sa::StringHash("OnKnockingDown");
inline constexpr sa::event_t OnHealingEvent = sa::StringHash("OnHealing");
inline constexpr sa::event_t OnStuckEvent = sa::StringHash("OnStuck");
inline constexpr sa::event_t OnIncMoraleEvent = sa::StringHash("OnIncMorale");
inline constexpr sa::event_t OnDecMoraleEvent = sa::StringHash("OnDecMorale");
inline constexpr sa::event_t OnKilledFoeEvent = sa::StringHash("OnKilledFoe");
inline constexpr sa::event_t OnXPAddedEvent = sa::StringHash("OnXPAdded");
inline constexpr sa::event_t OnSkillPointAddedEvent = sa::StringHash("OnSkillPointAdded");
inline constexpr sa::event_t OnAdvanceLevelEvent = sa::StringHash("OnAdvanceLevel");
inline constexpr sa::event_t OnAddEffectEvent = sa::StringHash("OnAddEffect");
inline constexpr sa::event_t OnKnockingDownTargetEvent = sa::StringHash("OnKnockingDownTarget");
inline constexpr sa::event_t OnInterruptingTargetEvent = sa::StringHash("OnInterruptingTarget");

using VoidVoidSignature = void(void);
using VoidIntSignature = void(int);
using VoidUInt32signature = void(uint32_t);
using OnCollideSignature = void(GameObject*);
using OnTriggerSignature = OnCollideSignature;
using OnLeftAreaSignatre = OnCollideSignature;
using OnInteractSignature = void(Actor*);
using OnResurrectSignature = OnInteractSignature;
using OnClickedSignature = OnInteractSignature;
using OnSelectedSignature = OnInteractSignature;

// OnStateChange(old, new)
using OnStateChangeSignature = void(AB::GameProtocol::CreatureState, AB::GameProtocol::CreatureState);
// OnAddEffect(source, effect, success)
using OnAddEffectSignature = void(Actor*, Effect*, bool&);
using OnAttackSuccessSignature = void(Actor*, DamageType, int32_t);
using OnInterruptingSkillSignature = void(Actor*, AB::Entities::SkillType, Skill*, bool&);
using OnPingObjectSignature = void(uint32_t, AB::GameProtocol::ObjectCallType, int);
using OnCanUseSkillSignature = void(Actor*, Skill*, bool&);
using OnGettingSkillTargetSignature = OnCanUseSkillSignature;
using OnAttackedSignature = void(Actor*, DamageType, int32_t, bool&);
using OnInterruptedSkillSignature = void(Skill*);
using OnStartUseSkillSignature = void(Actor*, Skill*);
using OnSkillTargetedSignature = OnStartUseSkillSignature;
using OnEndUseSkillSignature = OnStartUseSkillSignature;
// who died, killer
using OnActorDiedSignature = void(Actor*, Actor*);
// killer, who was killed
using OnKilledFoeSignature = OnActorDiedSignature;
using OnKnockingDownSignature = void(Actor*, uint32_t, bool&);
using OnKnockedDownSignature = VoidUInt32signature;
using OnHealingSignature = void(Actor*, int&);
using OnAttackSignature = void(Actor*, bool&);
using OnGetCriticalHitSignature = OnAttackSignature;
using OnGettingAttackedSignature = OnAttackSignature;
using OnInterruptingAttackSignature = void(bool&);
using OnResurrectedSignature = void(int, int);
using OnKnockingDownTargetSignature = void(Actor* source, uint32_t time, bool& success);
using OnInterruptingTargetSignature = void(Actor* source, bool& success);
using OnHandleCommandSignature = void(AB::GameProtocol::CommandType, const std::string&, Net::NetworkMessage&);

using GameObjectEvents = sa::Events<
    VoidVoidSignature,
    VoidUInt32signature,
    VoidIntSignature,
    OnInterruptingAttackSignature,
    OnStateChangeSignature,
    OnResurrectedSignature,
    OnCollideSignature,
    OnInteractSignature,
    OnInterruptedSkillSignature,
    OnStartUseSkillSignature,
    OnActorDiedSignature,
    OnKnockingDownSignature,
    OnAttackSuccessSignature,
    OnAttackedSignature,
    OnInterruptingSkillSignature,
    OnPingObjectSignature,
    OnCanUseSkillSignature,
    OnAttackSignature,
    OnHealingSignature,
    OnAddEffectSignature,
    OnKnockingDownTargetSignature,
    OnInterruptingTargetSignature,
    OnHandleCommandSignature
>;

// Events sent by the game
inline constexpr sa::event_t OnGameActorDiedEvent = sa::StringHash("OnGameActorDied");
inline constexpr sa::event_t OnGameActorResurrectedEvent = sa::StringHash("OnGameActorResurrected");

// who died, killer
using OnGameActorDiedSignature = void(Actor*, Actor*);
using OnGameActorResurrectedSignature = void(Actor*);

using GameEvents = sa::Events<
    OnGameActorResurrectedSignature,
    OnGameActorDiedSignature
>;

}
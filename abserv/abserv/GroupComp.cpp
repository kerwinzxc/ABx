/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "GroupComp.h"
#include "Actor.h"
#include "Group.h"
#include <AB/Packets/Packet.h>
#include <AB/Packets/ServerPackets.h>
#include "Party.h"

namespace Game::Components {

GroupComp::GroupComp(Actor& owner) :
    owner_(owner),
    groupColor_(TeamColor::Default)
{
}

GroupComp::~GroupComp()
{ }

void GroupComp::SetGroupColor(TeamColor value)
{
    if (value == groupColor_)
        return;
    groupColor_ = value;
    groupColorChanged_ = true;
}

void GroupComp::SetGroupMask(uint32_t value)
{
    if (groupMask_ == value)
        return;
    groupMask_ = value;
    groupMaskChanged_ = true;
}

void GroupComp::SetGroupId(uint32_t value)
{
    if (groupId_ == value)
        return;
    groupId_ = value;
    groupIdChanged_ = true;
}

void GroupComp::Update(uint32_t timeElapsed)
{
    if (auto* g = owner_.GetGroup())
    {
        // Call Group::Update() only once per tick
        if (g->IsLeader(owner_))
        {
            const bool oldDefeated = g->IsDefeated();
            const bool oldResigned = g->IsResigned();
            g->Update(timeElapsed);
            if (g->IsDefeated() && !oldDefeated)
                defeatedDirty_ = true;
            if (g->IsResigned() && !oldResigned)
                resignedDirty_ = true;
        }
    }
}

bool GroupComp::IsOwnerLeader() const
{
    if (auto* g = owner_.GetGroup())
        return g->IsLeader(owner_);
    return false;
}

void GroupComp::Write(Net::NetworkMessage& message)
{
    if (groupMaskChanged_ || groupColorChanged_)
    {
        message.AddByte(AB::GameProtocol::ServerPacketType::ObjectGroupChanged);
        AB::Packets::Server::ObjectGroupChanged packet;
        packet.id = owner_.id_;
        packet.validFields = 0;
        if (groupMaskChanged_)
        {
            packet.validFields |= AB::GameProtocol::ObjectGroupChangesFieldMask;
            packet.mask = groupMask_;
            groupMaskChanged_ = false;
        }
        if (groupColorChanged_)
        {
            packet.validFields |= AB::GameProtocol::ObjectGroupChangesFieldColor;
            packet.color = static_cast<uint8_t>(groupColor_);
            groupColorChanged_ = false;
        }
        if (groupIdChanged_)
        {
            packet.validFields |= AB::GameProtocol::ObjectGroupChangesFieldId;
            packet.groupId = groupId_;
            groupIdChanged_ = false;
        }
        AB::Packets::Add(packet, message);
    }

    if (auto* g = owner_.GetGroup())
    {
        // Write messages only once per tick
        if (!g->IsLeader(owner_))
            return;

        if (resignedDirty_)
        {
            message.AddByte(AB::GameProtocol::ServerPacketType::PartyResigned);
            AB::Packets::Server::PartyResigned packet = {
                g->GetId(),
                g->GetName()
            };
            AB::Packets::Add(packet, message);
            resignedDirty_ = false;
        }

        if (defeatedDirty_)
        {
            message.AddByte(AB::GameProtocol::ServerPacketType::PartyDefeated);
            AB::Packets::Server::PartyDefeated packet = {
                g->GetId(),
                g->GetName()
            };
            AB::Packets::Add(packet, message);
            defeatedDirty_ = false;
        }
    }
}

}

/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <sa/Noncopyable.h>
#include <abshared/Team.h>

namespace Game {
class Actor;
}

namespace Game::Components {

class GroupComp
{
    NON_COPYABLE(GroupComp)
    NON_MOVEABLE(GroupComp)
private:
    Actor& owner_;
    TeamColor groupColor_;
    /// Friend foe identification. Upper 16 bit is foe mask, lower 16 bit friend mask.
    /// This gives us 3 types of relation: (1) friend, (2) foe and (3) neutral
    /// and (4) in theory, both but that would be silly.
    uint32_t groupMask_{ 0 };
    // Group/Party ID
    uint32_t groupId_{ 0 };
    bool groupMaskChanged_{ false };
    bool groupColorChanged_{ false };
    bool groupIdChanged_{ false };
    bool resignedDirty_{ false };
    bool defeatedDirty_{ false };
    bool IsOwnerLeader() const;
public:
    explicit GroupComp(Actor& owner);
    ~GroupComp();

    TeamColor GetGroupColor() const { return groupColor_; }
    void SetGroupColor(TeamColor value);
    uint32_t GetGroupMask() const { return groupMask_; }
    void SetGroupMask(uint32_t value);
    uint32_t GetGroupId() const { return groupId_; }
    void SetGroupId(uint32_t value);
    void Update(uint32_t timeElapsed);
    void Write(Net::NetworkMessage& message);
};

}

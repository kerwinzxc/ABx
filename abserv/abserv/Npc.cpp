/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Npc.h"
#include "DamageComp.h"
#include "DataProvider.h"
#include "Game.h"
#include "GameManager.h"
#include "Group.h"
#include "Player.h"
#include "QuestComp.h"
#include "Script.h"
#include "ScriptManager.h"
#include <abai/BevaviorCache.h>
#include <sa/Assert.h>
#include <sa/TemplateParser.h>
#include <abscommon/DataClient.h>
#include "GroupComp.h"

namespace Game {

void Npc::InitializeLua()
{
    Lua::RegisterLuaAll(luaState_);
    luaState_["self"] = this;
    luaInitialized_ = true;
}

void Npc::RegisterLua(kaguya::State& state)
{
    // clang-format off
    state["Npc"].setClass(kaguya::UserdataMetatable<Npc, Actor>()
        .addFunction("IsServerOnly", &Npc::IsServerOnly)
        .addFunction("SetServerOnly", &Npc::SetServerOnly)

        .addFunction("SetLevel", &Npc::SetLevel)     // Can only be used in onInit(), i.e. before it is sent to the clients
        .addFunction("SetName", &Npc::_LuaSetName)   // After the spawn has been sent to the client it can not be changed
        .addFunction("Say", &Npc::Say)
        .addFunction("GetScriptFile", &Npc::_LuaGetScriptFile) // Script file is an indicator what type of NPC it is
        .addFunction("SayQuote", &Npc::SayQuote)
        .addFunction("Whisper", &Npc::Whisper)
        .addFunction("ShootAt", &Npc::ShootAt)
        .addFunction("SetBehavior", &Npc::SetBehavior)
        .addFunction("IsWander", &Npc::IsWander)
        .addFunction("SetWander", &Npc::SetWander)
        .addFunction("AddWanderPoint", &Npc::_LuaAddWanderPoint)
        .addFunction("AddWanderPoints", &Npc::_LuaAddWanderPoints)
        .addFunction("AddQuest", &Npc::_LuaAddQuest)
        .addFunction("GetNpcType", &Npc::_LuaGetNpcType)
        .addFunction("SetNpcType", &Npc::_LuaSetNpcType)
        .addFunction("GetCombatMode", &Npc::_LuaGetCombatMode)
        .addFunction("SetCombatMode", &Npc::_LuaSetCombatMode)
    );
    // clang-format on
}

Npc::Npc() :
    Actor(),
    luaInitialized_(false)
{
    events_.Subscribe<OnAttackSignature>(OnSourceAttackEvent, std::bind(&Npc::OnAttack, this, std::placeholders::_1, std::placeholders::_2));
    events_.Subscribe<OnAttackSuccessSignature>(OnSourceAttackSuccessEvent, std::bind(&Npc::OnAttackSuccess, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    events_.Subscribe<OnGettingAttackedSignature>(OnTargetAttackEvent, std::bind(&Npc::OnGettingAttacked, this, std::placeholders::_1, std::placeholders::_2));
    events_.Subscribe<OnAttackedSignature>(OnTargetAttackedEvent, std::bind(
        &Npc::OnAttacked, this,
        std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
    events_.Subscribe<OnInterruptingAttackSignature>(OnInterruptingAttackEvent, std::bind(&Npc::OnInterruptingAttack, this, std::placeholders::_1));
    events_.Subscribe<OnInterruptingSkillSignature>(OnInterruptingSkillEvent,
        std::bind(&Npc::OnInterruptingSkill, this,
            std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
    events_.Subscribe<OnGettingSkillTargetSignature>(OnGettingSkillTargetEvent, std::bind(&Npc::OnGettingSkillTarget, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    events_.Subscribe<OnCanUseSkillSignature>(OnCanUseSkillEvent, std::bind(&Npc::OnCanUseSkill, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    events_.Subscribe<OnActorDiedSignature>(OnDiedEvent, std::bind(&Npc::OnDied, this, std::placeholders::_1, std::placeholders::_2));
    events_.Subscribe<OnEndUseSkillSignature>(OnEndUseSkillEvent, std::bind(&Npc::OnEndUseSkill, this, std::placeholders::_1, std::placeholders::_2));
    events_.Subscribe<OnStartUseSkillSignature>(OnStartUseSkillEvent, std::bind(&Npc::OnStartUseSkill, this, std::placeholders::_1, std::placeholders::_2));
    events_.Subscribe<OnClickedSignature>(OnClickedEvent, std::bind(&Npc::OnClicked, this, std::placeholders::_1));
    events_.Subscribe<OnCollideSignature>(OnCollideEvent, std::bind(&Npc::OnCollide, this, std::placeholders::_1));
    events_.Subscribe<OnSelectedSignature>(OnSelectedEvent, std::bind(&Npc::OnSelected, this, std::placeholders::_1));
    events_.Subscribe<OnTriggerSignature>(OnTriggerEvent, std::bind(&Npc::OnTrigger, this, std::placeholders::_1));
    events_.Subscribe<OnLeftAreaSignatre>(OnLeftAreaEvent, std::bind(&Npc::OnLeftArea, this, std::placeholders::_1));
    events_.Subscribe<VoidVoidSignature>(OnArrivedEvent, std::bind(&Npc::OnArrived, this));
    events_.Subscribe<VoidVoidSignature>(OnInterruptedAttackEvent, std::bind(&Npc::OnInterruptedAttack, this));
    events_.Subscribe<OnInterruptedSkillSignature>(OnInterruptedSkillEvent, std::bind(&Npc::OnInterruptedSkill, this, std::placeholders::_1));
    events_.Subscribe<OnKnockedDownSignature>(OnKnockedDownEvent, std::bind(&Npc::OnKnockedDown, this, std::placeholders::_1));
    events_.Subscribe<VoidIntSignature>(OnHealedEvent, std::bind(&Npc::OnHealed, this, std::placeholders::_1));
    events_.Subscribe<OnResurrectedSignature>(OnResurrectedEvent, std::bind(&Npc::OnResurrected, this, std::placeholders::_1, std::placeholders::_2));
    events_.Subscribe<OnInteractSignature>(OnInteractEvent, std::bind(&Npc::OnInteract, this, std::placeholders::_1));
    // Party and Groups must be unique, i.e. share the same ID pool.
    groupComp_->SetGroupId(Group::GetNewId());
    InitializeLua();
}

Npc::~Npc()
{
    auto* group = GetGroup();
    if (group)
        group->Remove(id_);
}

bool Npc::LoadScript(const std::string& fileName)
{
    auto script = GetSubsystem<IO::DataProvider>()->GetAsset<Script>(fileName);
    if (!script)
        return false;
    if (!script->Execute(luaState_))
        return false;
    scriptFile_ = fileName;

    name_ = static_cast<const char*>(luaState_["name"]);
    if (Lua::IsNumber(luaState_, "level"))
        level_ = luaState_["level"];
    if (Lua::IsNumber(luaState_, "itemIndex"))
        itemIndex_ = luaState_["itemIndex"];
    if (Lua::IsNumber(luaState_, "sex"))
        sex_ = luaState_["sex"];
    if (Lua::IsNumber(luaState_, "interactionRange"))
        interactionRange_ = static_cast<Ranges>(luaState_["interactionRange"]);
    if (Lua::IsNumber(luaState_, "group_id"))
        groupComp_->SetGroupId(luaState_["group_id"]);
    if (Lua::IsBool(luaState_, "wander"))
        SetWander(luaState_("wander"));

    if (Lua::IsNumber(luaState_, "creatureState"))
        stateComp_.SetState(luaState_["creatureState"], true);
    else
        stateComp_.SetState(AB::GameProtocol::CreatureState::Idle, true);

    IO::DataClient* client = GetSubsystem<IO::DataClient>();

    if (Lua::IsNumber(luaState_, "prof1Index"))
    {
        skills_->prof1_.index = luaState_["prof1Index"];
        if (skills_->prof1_.index != 0)
        {
            if (!client->Read(skills_->prof1_))
            {
                LOG_WARNING << "Unable to read primary profession of " << GetName() << ", index = " << skills_->prof1_.index << std::endl;
            }
        }
    }
    if (Lua::IsNumber(luaState_, "prof2Index"))
    {
        skills_->prof2_.index = luaState_["prof2Index"];
        if (skills_->prof2_.index != 0)
        {
            if (!client->Read(skills_->prof2_))
            {
                LOG_WARNING << "Unable to read secondary profession of " << GetName() << ", index = " << skills_->prof2_.index << std::endl;
            }
        }
    }

    std::string bt;
    if (Lua::IsString(luaState_, "behavior"))
        bt = static_cast<const char*>(luaState_["behavior"]);
    if (Lua::IsFunction(luaState_, "onUpdate"))
        sa::bits::set(functions_, FunctionUpdate);
    if (Lua::IsFunction(luaState_, "onTrigger"))
        sa::bits::set(functions_, FunctionOnTrigger);
    if (Lua::IsFunction(luaState_, "onLeftArea"))
        sa::bits::set(functions_, FunctionOnLeftArea);
    if (Lua::IsFunction(luaState_, "onGetQuote"))
        sa::bits::set(functions_, FunctionOnGetQuote);
    if (Lua::IsFunction(luaState_, "getAttackDamage"))
        sa::bits::set(functions_, FunctionGetAttackDamage);
    if (Lua::IsFunction(luaState_, "getDamageType"))
        sa::bits::set(functions_, FunctionGetDamageType);
    if (Lua::IsFunction(luaState_, "getAttackSpeed"))
        sa::bits::set(functions_, FunctionGetAttackSpeed);
    if (Lua::IsFunction(luaState_, "getArmorEffect"))
        sa::bits::set(functions_, FunctionGetArmorEffect);
    if (Lua::IsFunction(luaState_, "getBaseArmor"))
        sa::bits::set(functions_, FunctionGetBaseArmor);
    if (Lua::IsFunction(luaState_, "onEnterRange"))
        sa::bits::set(functions_, FunctionOnEnterRange);
    if (Lua::IsFunction(luaState_, "onLeaveRange"))
        sa::bits::set(functions_, FunctionOnLeaveRange);

    if (Lua::IsFunction(luaState_, "getSellingItemTypes"))
    {
        std::vector<uint32_t> types = luaState_["getSellingItemTypes"]();
        for (auto type : types)
        {
            sellItemTypes_.emplace(static_cast<AB::Entities::ItemType>(type));
        }
    }

    GetSkillBar()->InitAttributes();
    // Initialize resources, etc. may be overwritten in onInit() in the NPC script bellow.
    Initialize();

    if (!bt.empty())
        SetBehavior(bt);

    return luaState_["onInit"]();
}

void Npc::SetLevel(uint32_t value)
{
    level_ = value;
    resourceComp_->UpdateResources();
}

void Npc::ObjectEnteredRange(Ranges range, uint32_t objectId)
{
    if (!HaveFunction(FunctionOnEnterRange))
        return;
    auto game = GetGame();
    if (!game)
        return;
    auto* actor = game->GetObject<Actor>(objectId);
    if (!actor)
        return;
    luaState_["onEnterRange"](range, actor);
}

void Npc::ObjectLeftRange(Ranges range, uint32_t objectId)
{
    if (!HaveFunction(FunctionOnLeaveRange))
        return;
    auto game = GetGame();
    if (!game)
        return;
    auto* actor = game->GetObject<Actor>(objectId);
    if (!actor)
        return;
    luaState_["onLeaveRange"](range, actor);
}

void Npc::Update(uint32_t timeElapsed, Net::NetworkMessage& message)
{
    // I think first we should run the BT
    if (aiComp_)
        aiComp_->Update(timeElapsed);
    if (wanderComp_)
        wanderComp_->Update(timeElapsed);

    Actor::Update(timeElapsed, message);

    if (HaveFunction(FunctionUpdate))
        luaState_["onUpdate"](timeElapsed);

    Lua::CollectGarbage(luaState_);
}

bool Npc::SetBehavior(const std::string& name)
{
    auto* cache = GetSubsystem<AI::BevaviorCache>();
    auto root = cache->Get(name);
    if (!root)
    {
        aiComp_.reset();
        if (!name.empty())
            LOG_WARNING << "Behavior with name " << name << " not found in cache" << std::endl;
        return false;
    }
    aiComp_ = ea::make_unique<Components::AiComp>(*this);
    aiComp_->GetAgent().SetBehavior(root);
    return true;
}

float Npc::GetAggro(const Actor* other)
{
    if (!other || !IsEnemy(other))
        return 0.0f;

    auto* random = GetSubsystem<Crypto::Random>();
    const float dist = 1.0f / (GetPosition().Distance(other->GetPosition()) / RANGE_AGGRO);
    const float health = 1.0f - other->resourceComp_->GetHealthRatio();
    const float ld = damageComp_->IsLastDamager(*other) ? 2.0f : 1.0f;
    const float rval = random->GetFloat();
    return ((dist + health) * ld) * rval;
}

bool Npc::GetSkillCandidates(
    ea::vector<int>& results,
    SkillEffect effect, SkillEffectTarget target,
    AB::Entities::SkillType interrupts /* = AB::Entities::SkillTypeAll */,
    const Actor* targetActor /* = nullptr */)
{
    // skill index -> cost (smaller is better)
    ea::map<int, float> sorting;
    skills_->VisitSkills([&](int index, const Skill& current)
    {
        if (!current.CanUseOnTarget(*this, targetActor))
            return Iteration::Continue;
        if (current.NeedsTarget())
        {
            // If there is a target check if it's in range
            if (targetActor && !IsInRange(current.GetRange(), targetActor))
                return Iteration::Continue;
        }
        if (!current.HasEffect(effect))
            return Iteration::Continue;
        if (target != SkillTargetNone && !current.HasTarget(target))
            return Iteration::Continue;
        if (!current.IsRecharged())
            return Iteration::Continue;
        if (!resourceComp_->HaveEnoughResources(&current))
            return Iteration::Continue;
        if (effect == SkillEffect::SkillEffectInterrupt &&
            interrupts != AB::Entities::SkillTypeAll &&
            !current.CanInterrupt(interrupts))
            // If this is an interrupt skill, and interrupts argument was passed, check also that
            return Iteration::Continue;

        results.push_back(index);
        // Calculate some score, depending on activation time, costs.... Smaller is better
        sorting[index] = current.CalculateCost([this, &current](CostType type)
        {
            switch (type)
            {
            case CostType::Activation:
                return 0.2f;
            case CostType::Recharge:
                return 0.0f;
            case CostType::Energy:
            {
                const float er = resourceComp_->GetEnergyRatio();
                return 1.0f - er;
            }
            case CostType::Adrenaline:
            {
                const int a = resourceComp_->GetAdrenaline();
                return static_cast<float>(a) / static_cast<float>(current.adrenaline_);
            }
            case CostType::HpSacrify:
            {
                const float hr = resourceComp_->GetHealthRatio();
                return 1.0f - hr;
            }
            }
            return 0.0f;
        });
        // Prefer Elite skills
        if (current.elite_)
            sorting[index] = sorting[index] * 0.5f;
        else
        {
            auto* rng = GetSubsystem<Crypto::Random>();
            sorting[index] = sorting[index] * rng->Get<float>(0.6f, 1.2f);
        }
        return Iteration::Continue;
    });

    if (results.size() == 0)
        return false;

    ea::sort(results.begin(), results.end(), [&sorting](int i, int j)
    {
        return sorting[i] < sorting[j];
    });
    return true;
}

int Npc::GetBestSkillIndex(SkillEffect effect, SkillEffectTarget target,
    AB::Entities::SkillType interrupts /* = AB::Entities::SkillTypeAll */,
    const Actor* targetActor /* = nullptr */)
{
    ea::vector<int> skillIndices;
    if (GetSkillCandidates(skillIndices, effect, target, interrupts, targetActor))
        return *skillIndices.begin();
    return -1;
}

bool Npc::IsWander() const
{
    return !!wanderComp_;
}

void Npc::SetWander(bool value)
{
    if (!value)
    {
        if (wanderComp_)
            wanderComp_.reset();
        return;
    }

    if (!wanderComp_)
        wanderComp_ = ea::make_unique<Components::WanderComp>(*this);
}

uint32_t Npc::GetAttackSpeed()
{
    if (HaveFunction(FunctionGetAttackSpeed))
        return static_cast<uint32_t>(luaState_["getAttackSpeed"]());
    return Actor::GetAttackSpeed();
}

DamageType Npc::GetAttackDamageType()
{
    if (HaveFunction(FunctionGetDamageType))
        return static_cast<DamageType>(luaState_["getDamageType"]());
    return Actor::GetAttackDamageType();
}

int32_t Npc::GetAttackDamage(bool critical)
{
    if (HaveFunction(FunctionGetAttackDamage))
        return luaState_["getAttackDamage"](critical);
    return Actor::GetAttackDamage(critical);
}

float Npc::GetArmorEffect(DamageType damageType, DamagePos pos, float penetration)
{
    if (HaveFunction(FunctionGetArmorEffect))
        return luaState_["getArmorEffect"](damageType, pos, penetration);
    return Actor::GetArmorEffect(damageType, pos, penetration);
}

int Npc::GetBaseArmor(DamageType damageType, DamagePos pos)
{
    if (HaveFunction(FunctionGetBaseArmor))
        return luaState_["getBaseArmor"](damageType, pos);
    // Armor base comes from the armor the actor wears, but NPCs do not wear armors.
    return GetLevel() * 3;
}

void Npc::WriteSpawnData(Net::NetworkMessage& msg)
{
    if (!serverOnly_)
        Actor::WriteSpawnData(msg);
}

void Npc::Say(ChatType channel, const std::string& message)
{
    switch (channel)
    {
    case ChatType::Map:
    {
        ea::shared_ptr<ChatChannel> ch = GetSubsystem<Chat>()->Get(ChatType::Map, static_cast<uint64_t>(GetGame()->id_));
        if (ch)
            ch->TalkNpc(*this, message);
        break;
    }
    case ChatType::Party:
    {
        ea::shared_ptr<ChatChannel> ch = GetSubsystem<Chat>()->Get(ChatType::Party, GetGroupId());
        if (ch)
            ch->TalkNpc(*this, message);
        break;
    }
    default:
        // N/A
        break;
    }
}

std::string Npc::GetQuote(int index)
{
    if (!HaveFunction(FunctionOnGetQuote))
        return "";
    const char* q = static_cast<const char*>(luaState_["onGetQuote"](index));
    return q;
}

void Npc::_LuaSetName(const std::string& name)
{
    name_ = name;
}

void Npc::_LuaAddWanderPoint(const Math::StdVector3& point)
{
    if (IsWander())
        wanderComp_->AddRoutePoint(point);
}

void Npc::_LuaAddWanderPoints(const std::vector<Math::StdVector3>& points)
{
    for (const auto& point : points)
        _LuaAddWanderPoint(point);
}

void Npc::_LuaSetNpcType(int type)
{
    npcType_ = static_cast<AB::GameProtocol::NpcType>(type);
}

int Npc::_LuaGetNpcType() const
{
    return static_cast<int>(npcType_);
}

void Npc::_LuaSetCombatMode(int value)
{
    if (value < static_cast<int>(CombatMode::Fight) || value > static_cast<int>(CombatMode::AvoidCombat))
        return;
    combatMode_ = static_cast<CombatMode>(value);
}

std::string Npc::_LuaGetScriptFile() const
{
    return scriptFile_;
}

bool Npc::SayQuote(ChatType channel, int index)
{
    const std::string quote = GetQuote(index);
    if (quote.empty())
        return false;

    const std::string t = sa::templ::Parser::Evaluate(quote, [this](const sa::templ::Token& token) -> std::string
    {
        switch (token.type)
        {
        case sa::templ::Token::Type::Variable:
            if (token.value == "selected_name")
            {
                if (auto sel = GetSelectedObject())
                    return sel->GetName();
                return "";
            }
            LOG_WARNING << "Not implemented placeholder " << token.value << std::endl;
            return "";
        default:
            return token.value;
        }
    });

    Say(channel, t);
    return true;
}

void Npc::Whisper(Player* player, const std::string& message)
{
    if (!player)
        return;

    ea::shared_ptr<ChatChannel> ch = GetSubsystem<Chat>()->Get(ChatType::Whisper, static_cast<uint64_t>(player->id_));
    if (ch)
        ch->TalkNpc(*this, message);
}

void Npc::ShootAt(const std::string& itemUuid, Actor* target)
{
    ASSERT(HasGame());
    GetGame()->AddProjectile(itemUuid, GetPtr<Actor>(), target->GetPtr<Actor>());
}

void Npc::OnSelected(Actor* selector)
{
    if (luaInitialized_ && selector)
        Lua::CallFunction(luaState_, "onSelected", selector);
}

void Npc::OnClicked(Actor* selector)
{
    if (luaInitialized_ && selector)
        Lua::CallFunction(luaState_, "onClicked", selector);
    if (Is<Player>(selector))
    {
        if (!IsInRange(Ranges::Adjecent, selector))
            return;
        // Get quests for this player
        auto& player = To<Player>(*selector);
        const auto quests = GetQuestsForPlayer(player);
        player.TriggerQuestSelectionDialog(id_, quests);
    }
}

void Npc::OnArrived()
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onArrived");
}

void Npc::OnCollide(GameObject* other)
{
    if (luaInitialized_ && other)
        Lua::CallFunction(luaState_, "onCollide", other);
}

void Npc::OnTrigger(GameObject* other)
{
    if (HaveFunction(FunctionOnTrigger))
        Lua::CallFunction(luaState_, "onTrigger", other);
}

void Npc::OnLeftArea(GameObject* other)
{
    if (HaveFunction(FunctionOnLeftArea))
        Lua::CallFunction(luaState_, "onLeftArea", other);
}

void Npc::OnEndUseSkill(Actor* target, Skill* skill)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onEndUseSkill", target, skill);
}

void Npc::OnStartUseSkill(Actor* target, Skill* skill)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onStartUseSkill", target, skill);
}

void Npc::OnAttack(Actor* target, bool& canAttack)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onAttack", target, canAttack);
}

void Npc::OnAttackSuccess(Actor* target, DamageType type, int32_t damage)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onAttackSuccess", target, type, damage);
}

void Npc::OnAttacked(Actor* source, DamageType type, int32_t damage, bool& canGetAttacked)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onAttacked", source, type, damage, canGetAttacked);
}

void Npc::OnGettingAttacked(Actor* source, bool& canGetAttacked)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onGettingAttacked", source, canGetAttacked);
}

void Npc::OnCanUseSkill(Actor* target, Skill* skill, bool& success)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onCanUseSkill", target, skill, success);
}

void Npc::OnGettingSkillTarget(Actor* source, Skill* skill, bool& success)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onGettingSkillTarget", source, skill, success);
}

void Npc::OnInteract(Actor* actor)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onInteract", actor);
}

void Npc::OnInterruptingAttack(bool& success)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onInterruptingAttack", success);
}

void Npc::OnInterruptingSkill(Actor* source, AB::Entities::SkillType type, Skill* skill, bool& success)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onInterruptingSkill", source, type, skill, success);
}

void Npc::OnInterruptedAttack()
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onInterruptedAttack");
}

void Npc::OnInterruptedSkill(Skill* skill)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onInterruptedSkill", skill);
}

void Npc::OnKnockedDown(uint32_t time)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onKnockedDown", time);
}

void Npc::OnHealed(int hp)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onHealed", hp);
}

void Npc::OnDied(Actor*, Actor* killer)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onDied", killer);
}

void Npc::OnResurrected(int, int)
{
    if (luaInitialized_)
        Lua::CallFunction(luaState_, "onResurrected");
}

void Npc::_LuaAddQuest(uint32_t index)
{
    quests_.emplace(index);
}

ea::set<uint32_t> Npc::GetQuestsForPlayer(const Player& player) const
{
    ea::set<uint32_t> result;
    const auto& qc = *player.questComp_;
    for (auto qIndex : quests_)
    {
        if (qc.IsAvailable(qIndex))
            result.emplace(qIndex);
    }
    return result;
}

bool Npc::HaveQuestsForPlayer(const Player& player) const
{
    const auto& qc = *player.questComp_;
    for (auto qIndex : quests_)
    {
        if (qc.IsAvailable(qIndex))
            return true;
    }
    return false;
}

bool Npc::IsSellingItemType(AB::Entities::ItemType type) const
{
    return sellItemTypes_.find(type) != sellItemTypes_.end();
}

bool Npc::IsSellingItem(uint32_t itemIndex)
{
    if (!Lua::IsFunction(luaState_, "isSellingItem"))
        return false;
    const bool result = luaState_["isSellingItem"](itemIndex);
    return result;
}

}

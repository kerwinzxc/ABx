/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "AiGotoRandomPos.h"
#include "../Npc.h"
#include "../AiAgent.h"
#include <abshared/Mechanic.h>
#include <abscommon/Subsystems.h>
#include <abscommon/Random.h>
#include <abscommon/Logger.h>
#include "../Group.h"

//#define DEBUG_AI

namespace AI {
namespace Actions {

Math::Vector3 GotoRandomPos::GetAimPos(Agent& agent)
{
    Game::Npc& npc = GetNpc(agent);
    auto master = npc.GetMaster();
    if (master)
    {
        const auto& masterPos = master->GetPosition();
        if (masterPos.DistanceXZ(npc.GetPosition()) > Game::AT_POSITION_THRESHOLD)
            return masterPos;
    }

    Game::Group* group = npc.GetGroup();
    if (group && group->GetLeader() && group->GetLeader() != &npc)
    {

        // If we are in a crowd and we are not the leader, follow the leader
        Game::Actor* leader = group->GetLeader();
        const auto& leaderpos = leader->GetPosition();
        if (leaderpos.DistanceXZ(npc.GetPosition()) > Game::AT_POSITION_THRESHOLD)
            return leaderpos;
    }

    Math::Vector3 result = npc.GetPosition();
    auto* rng = GetSubsystem<Crypto::Random>();
    result += {
        Math::Lerp(-8.0f, 8.0f, rng->GetFloat()),
            0.0f,
            Math::Lerp(-8.0f, 8.0f, rng->GetFloat())
    };
    return result;
}

Node::Status GotoRandomPos::DoAction(Agent& agent, uint32_t)
{
    Game::Npc& npc = GetNpc(agent);
    if (npc.IsImmobilized())
        return Status::Failed;
    auto& aiAgent = GetAgent(agent);
    Math::Vector3 pos;
    if (aiAgent.aiContext_.Has<Math::Vector3>(id_))
        pos = aiAgent.aiContext_.Get<Math::Vector3>(id_);
    else
    {
        pos = GetAimPos(agent);
        aiAgent.aiContext_.Set<Math::Vector3>(id_, pos);
#ifdef DEBUG_AI
        LOG_DEBUG << "New pos is " << pos << std::endl;
#endif
    }

    if (pos.Equals(npc.GetPosition(), Game::AT_POSITION_THRESHOLD))
    {
        aiAgent.aiContext_.Delete<Math::Vector3>(id_);
        return Status::Finished;
    }

    if (IsCurrentAction(agent) && npc.stateComp_.GetState() == AB::GameProtocol::CreatureState::Moving)
        return Status::Running;

    if (npc.GotoPosition(pos))
    {
        npc.SetSpeed(0.3f);
#ifdef DEBUG_AI
        LOG_DEBUG << npc.GetName() << " goes from " << npc.GetPosition().ToString() <<
                     " to " << pos.ToString() << std::endl;
#endif
        return Status::Running;
    }
#ifdef DEBUG_AI
//    LOG_DEBUG << "Failed to go to " << home << std::endl;
#endif
    aiAgent.aiContext_.Delete<Math::Vector3>(id_);
    return Status::Finished;
}

GotoRandomPos::GotoRandomPos(const ArgumentsType& arguments) :
    Action(arguments)
{ }

}
}

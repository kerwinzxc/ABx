/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "AiIsCombatMode.h"
#include "../AiAgent.h"
#include <abscommon/Logger.h>

namespace AI {
namespace Conditions {

IsCombatMode::IsCombatMode(const ArgumentsType& arguments) :
    Condition(arguments)
{
    GetArgument<Game::Npc::CombatMode>(arguments, 0, mode_);
}

bool IsCombatMode::Evaluate(Agent& agent, const Node&)
{
    Game::Npc& npc = GetNpc(agent);
//    LOG_DEBUG << (npc.combatMode_ == mode_) << std::endl;
    return npc.combatMode_ == mode_;
}

}
}

-- By Koudelka

include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

-- TODO: Make an item for it
itemIndex = 0
effect = SkillEffectProtect
effectTarget = SkillTargetAoe

local reduce = 0
local lifetime = 0

function onInit()
  local source = self:GetSource()
  if (source == nil) then
    return false
  end

  local attribVal = source:GetAttributeRank(ATTRIB_EARTH)
  local factor = (10 - 5) / 12
  reduce = attribVal * factor + 5
  self:SetRange(RANGE_ADJECENT)
  lifetime = attribVal * (17 - 5) / 12 + 5
  self:SetLifetime(lifetime)
  self:SetRange(RANGE_ADJECENT)
  self:SetTrigger(true)

  return true
end

function onTrigger(other)
  local actor = other:AsActor()
  if (actor == nil) then
    return
  end
  if (not actor:IsDead() and self:IsAlly(actor) and actor:GetSpecies() ~= SPECIES_SPIRIT) then
    other:AddEffect(self:GetSource(), 5017, lifetime)
  end
end

function onLeftArea(other)
  local actor = other:AsActor()
  if (actor == nil) then
    return
  end
  if (not actor:IsDead() and self:IsAlly(actor) and actor:GetSpecies() ~= SPECIES_SPIRIT) then
    other:RemoveEffect(5017)
  end
end

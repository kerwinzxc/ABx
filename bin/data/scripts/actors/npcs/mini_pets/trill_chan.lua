include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

name = "Trill"
itemIndex = 19
sex = SEX_FEMALE
creatureState = CREATURESTATE_IDLE
prof1Index = PROFESSIONINDEX_NONE
prof2Index = PROFESSIONINDEX_NONE
behavior = "mini_pet"

local lastPosUpdate = 0
local lastRegenUpdate = 0

function onInit()
  self:SetResource(RESOURCE_TYPE_MAXHEALTH, SETVALUE_TYPE_ABSOLUTE, (self:GetLevel() * 20) + 80)
  self:SetResurrectable(false)
  self:SetRecycleable(false)
  self:SetUndestroyable(true)
  self:SetGroupId(0)
  self:SetCollisionMask(0)
  self:SetCollisionLayer(0)
  local master = self:GetMaster()
  if (master ~= nil) then
    self:SetHomePos(master:GetPosition())
  end
  return true
end

function onUpdate(timeElapsed)
  if (self:IsDead()) then
    return
  end
  lastRegenUpdate = lastRegenUpdate + timeElapsed
  lastPosUpdate = lastPosUpdate + timeElapsed
  if (lastRegenUpdate >= 20000) then
    self:SetHealthRegen(-1)
    lastRegenUpdate = 0
  end
  local master = self:GetMaster()
  if (master == nil) then
    self:Remove()
    return
  end
  
  if (lastPosUpdate >= 1000) then
    local pos = self:GetPosition()
    if (master ~= nil) then
      self:SetHomePos(master:GetPosition())
    end
    lastPosUpdate = 0
  end
end

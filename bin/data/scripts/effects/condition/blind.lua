include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

index = 10005
name = "Blind"
icon = "Textures/Skills/placeholder.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryCondition
soundEffect = ""
particleEffect = ""

function onAttacked(source, target, damageType, damage)
  source:SetAttackError(ATTACK_ERROR_TARGET_MISSED)
  return false
end

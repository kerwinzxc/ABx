include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

index = 10004
name = "Burning"
icon = "Textures/Effects/fire.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryCondition
soundEffect = ""
particleEffect = ""

isPersistent = false

function getDuration(source, target)
  return 0
end

function onStart(source, target)
  target:SetHealthRegen(-7)
  return true
end

function onEnd(source, target)
  target:SetHealthRegen(7)
end

-- Effect was removed before ended
function onRemove(source, target)
  target:SetHealthRegen(7)
end

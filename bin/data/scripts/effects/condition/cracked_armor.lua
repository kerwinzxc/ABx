include("/scripts/includes/skill_consts.lua")

index = 10007
name = "Poison"
icon = "Textures/Skills/placeholder.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryCondition
soundEffect = ""
particleEffect = ""

isPersistent = false

function getArmor(type, value)
  return math.floor(math.max(0, value - 20))
end

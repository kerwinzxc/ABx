include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

index = 10001
name = "Crippled"
icon = "Textures/Skills/placeholder.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryCondition
soundEffect = ""
particleEffect = ""

isPersistent = false

-- 2 Sec
local factor = -0.5

function getDuration(source, target)
  return 2000
end

function onStart(source, target)
  target:AddSpeed(factor)
  return true
end

function onEnd(source, target)
  target:AddSpeed(-factor)
end

-- Effect was removed before ended
function onRemove(source, target)
  target:AddSpeed(-factor)
end

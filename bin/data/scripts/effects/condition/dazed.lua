include("/scripts/includes/skill_consts.lua")

index = 10003
name = "Dazed"
icon = "Textures/Effects/dazed.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryCondition
soundEffect = ""
particleEffect = ""

function getSkillCost(skill, activation, energy, adrenaline, overcast, hp)
  return math.floor(activation * 2), energy, adrenaline, overcast, hp
end

function onGettingAttacked(source, target)
  target:InterruptSkill(source, SkillTypeSkill)
end

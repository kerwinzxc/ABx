include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

index = 10008
name = "Deep Wound"
icon = "Textures/Skills/placeholder.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryCondition
soundEffect = ""
particleEffect = ""

isPersistent = false

function onStart(source, target)
  return true
end

function getResources(maxHealth, maxEnergy)
  local reduce = maxHealth * 0.2
  if (reduce > 100) then
    reduce = 100
  end
  return maxHealth - reduce, maxEnergy
end

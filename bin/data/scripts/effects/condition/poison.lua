include("/scripts/includes/skill_consts.lua")

index = 10002
name = "Poison"
icon = "Textures/Effects/poison.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryCondition
soundEffect = ""
particleEffect = ""

isPersistent = false

function getDuration(source, target)
  return 0
end

function onStart(source, target)
  target:SetHealthRegen(-4)
  return true
end

function onEnd(source, target)
  target:SetHealthRegen(4)
end

-- Effect was removed before ended
function onRemove(source, target)
  target:SetHealthRegen(4)
end

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5207
name = "Avenger's Prayer"
icon = "Textures/Skills/Avenger's Prayer.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local healing = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_HEALING)
  healing = math.floor(attrib * (7 / 12) + 0.5)
  return true
end

-- TODO: Can't do this yet

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5008
name = "Basalt Bastion"
icon = "Textures/Skills/Basalt Bastion.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local factor = 0

function onStart(source, target)
  local attribVal = source:GetAttributeRank(ATTRIB_EARTH)
  factor = (attribVal * ((30 - 25) / 12) + 25) / 100
  target:AddSpeed(-factor)
  return true
end

function onEnd(source, target)
  target:AddSpeed(-factor)
end

function onRemove(source, target)
  target:AddSpeed(-factor)
end

function onInterruptingSkill(source, target, type, skill)
  return false
end


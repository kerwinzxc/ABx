-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5010
name = "Conjure Earth"
icon = "Textures/Skills/Conjure Earth.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local damageBonus = 0

function onStart(source, target)
  local attribVal = source:GetAttributeRank(ATTRIB_EARTH)
  damageBonus = math.floor(earth * ((17 - 5) / 12) + 12)
  return true
end

function getAttackDamage(value)
  -- TODO: Check for earth weapon
  return value + damageBonus
end

-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5201
name = "Divine Perfection"
icon = "Textures/Skills/Divine Perfection.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local increase = 0

function onStart(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_DEVINE_FAVOUR)
  increase = math.floor(attrib * (1 / 12) + 0.5)
  return true
end

function onStart(source, target)
  local sb = source:GetSkillBar()
  sb:SetAttributeRank(ATTRIB_HEALING, sb:GetAttributeRank(ATTRIB_HEALING) + increase)
  sb:SetAttributeRank(ATTRIB_SMITING, sb:GetAttributeRank(ATTRIB_SMITING) + increase)
  sb:SetAttributeRank(ATTRIB_PROTECTION, sb:GetAttributeRank(ATTRIB_PROTECTION) + increase)
  sb:SetAttributeRank(ATTRIB_DEVINE_FAVOUR, sb:GetAttributeRank(ATTRIB_DEVINE_FAVOUR) + increase)
end

function onEnd(source, target)
  local sb = source:GetSkillBar()
  sb:SetAttributeRank(ATTRIB_HEALING, sb:GetAttributeRank(ATTRIB_HEALING) - increase)
  sb:SetAttributeRank(ATTRIB_SMITING, sb:GetAttributeRank(ATTRIB_SMITING) - increase)
  sb:SetAttributeRank(ATTRIB_PROTECTION, sb:GetAttributeRank(ATTRIB_PROTECTION) - increase)
  sb:SetAttributeRank(ATTRIB_DEVINE_FAVOUR, sb:GetAttributeRank(ATTRIB_DEVINE_FAVOUR) - increase)
end

function onUpdate(source, target, time)
  if (source:GetResource(RESOURCE_TYPE_HEALTH) ~= source:GetResource(RESOURCE_TYPE_MAXHEALTH)) then
    source:RemoveEffect(self:Index())
  end
end

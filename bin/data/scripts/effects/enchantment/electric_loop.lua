-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5002
name = "Electric Loop"
icon = "Textures/Skills/Electric Loop.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

local damage
local lastDamage

function onStart(source, target)
  -- for 1...12...16

  local attribVal = target:GetAttributeRank(ATTRIB_AIR)
  local dmgfactor = (12 - 1) / 12
  damage = attribVal * dmgfactor + 1
  lastDamage = Tick()
  return true
end

function onUpdate(source, target, time)
  local tick = Tick()
  if (target == nil) then
    print("target = nil")
    return
  end
  if (tick - lastDamage >= 1000) then
    local actors = target:GetActorsInRange(RANGE_TOUCH)
    for i, actor in ipairs(actors) do
      if (actor:IsEnemy(target)) then
        actor:Damage(target, self:Index(), DAMAGETYPE_LIGHTNING, damage, 0.25)
      end
    end
    lastDamage = tick
  end
end

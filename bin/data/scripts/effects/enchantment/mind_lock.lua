-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5013
name = "Mind Lock"
icon = "Textures/Skills/Mind Lock.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

function onStart(source, target)
  return true
end

-- source adds the effect to target
function onAddEffect(source, target, effect)
  if (effect:GetCategory() == EffectCategoryHex) then
    return false
  end
  return true
end


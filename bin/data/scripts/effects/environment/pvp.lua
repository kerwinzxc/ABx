include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")

index = 1000
name = "PvP"
icon = "Textures/Effects/pvp.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnvironment
soundEffect = ""
particleEffect = ""

-- PvP is forever on this map
isPersistent = false

function getDuration(source, target)
  return TIME_FOREVER
end

function onStart(source, target)
  return true
end

function onEnd(source, target)
end

function onRemove(source, target)
end

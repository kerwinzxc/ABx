-- By Koudelka
include("/scripts/includes/skill_consts.lua")

isPersistent = false

index = 5000
name = "Bluster"
icon = "Textures/Skills/Bluster.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

function getAttackSpeed(weapon, value)
  return math.floor(value * 0.75)
end

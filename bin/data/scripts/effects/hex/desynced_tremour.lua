-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5011
name = "Desynced Tremour"
icon = "Textures/Skills/Desynced Tremour.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

function onStart(source, target)
  return true
end

function onUpdate(source, target, time)
  if (target:IsMoving()) then
    self:Remove()
  end
end

function onEnd(source, target)
  -- No effect when it was removed before ended
  target:KnockDown(source, 2000)
end


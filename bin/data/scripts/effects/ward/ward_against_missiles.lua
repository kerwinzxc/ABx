-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5018
name = "Ward Against Missiles"
icon = "Textures/Skills/Ward Against Missiles.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryWard

function onStart(source, target)
  return true
end

function onGettingAttacked(source, target)
  if (target:GetSpecies() ~= SPECIES_SPIRIT) then
    local r = Random()
    return r >= 0.5
  end
  return true
end

include("/scripts/includes/consts.lua")
include("/scripts/includes/item_functions.lua")

materialStats = {}
materialStats[1] = { 9999999, 1000 }
materialStats[2] = { 0, 0 }
materialStats[3] = { 0, 0 }
materialStats[4] = { 0, 0 }

function onDoubleClick(source)
  local SCRIPT = "/scripts/actors/npcs/mini_pets/trill_chan.lua"
  local currentMiniId = source:GetVarNumber("current_mini")
  local currentMiniScript = ""
  if (currentMiniId ~= 0) then
    local currentMini = source:GetGame():GetObject(currentMiniId)    
    if (currentMini ~= nil) then
      local npc = currentMini:AsNpc()
      currentMiniScript = npc:GetScriptFile()
      if (npc ~= nil) then
        npc:Remove()
        source:SetVarNumber("current_mini", 0)
      end
    end
  end
  if (currentMiniScript ~= SCRIPT) then
    local mini = source:SpawnSlave(SCRIPT, SLAVE_KIND_MINI_PET, 1, source:GetPosition())
    source:SetVarNumber("current_mini", mini:GetId())
  end
end

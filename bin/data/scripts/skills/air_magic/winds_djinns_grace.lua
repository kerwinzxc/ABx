-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5007
name = "Wind Djinn's Grace"
attribute = ATTRIB_AIR
skillType = SkillTypeEnchantment
isElite = true
description = "Elite Enchantment Spell: For ${floor(earth * (40 - 1) / 12 + 1)} seconds, you move and cast Air Magic spells 25% faster. Every second you are moving, you gain 1 Energy per 10 ranks of Energy Storage. Your armour is set to 10...45...60 and cannot be increased."
shortDescription = "Elite Enchantment Spell: For 1...40...45 seconds, you move and cast Air Magic spells 25% faster. Every second you are moving, you gain 1 Energy per 10 ranks of Energy Storage. Your armour is set to 10...45...60 and cannot be increased."
icon = "Textures/Skills/Wind Djinn's Grace.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 25
costAdrenaline = 0
activation = 250
recharge = 20000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectFasterCast | SkillEffectGainEnergy
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  local attrib = source:GetAttributeRank(ATTRIB_AIR)
  local time = math.floor(attrib * (40 - 1) / 12 + 1)
  source:AddEffect(source, self:Index(), time)
  return SkillErrorNone
end

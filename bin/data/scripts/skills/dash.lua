include("/scripts/includes/consts.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")

index = 1043
name = "Dash"
attribute = ATTRIB_NONE
skillType = SkillTypeStance
isElite = false
description = "Stance. You move 50% faster (3 seconds)."
shortDescription = "Stance. You move 50% faster (3 seconds)."
icon = "Textures/Skills/dash.png"
profession = PROFESSIONINDEX_NONE
soundEffect = "Sounds/FX/Effects/Dash.wav"
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 0
recharge = 8000
overcast = 0
effect = SkillEffecSpeed
effectTarget = SkillTargetSelf

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (source:IsDead()) then
    return SkillErrorInvalidTarget
  end
  
  -- Effect has the same index as the skill
  source:AddEffect(source, self:Index(), 0)
  return SkillErrorNone
end

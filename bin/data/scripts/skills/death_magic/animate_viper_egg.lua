-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5325
name = "Animate Viper Egg"
attribute = ATTRIB_DEATH
skillType = SkillTypeSpell
isElite = false
-- TODO: Make the minion cast some death magic spell, which simply deal some damage
description = "Spell: Exploit the nearest corpse to create a level ${floor(death * (13 / 12) + 1)} Viper Egg minion. When Viper Egg dies, it is replaced by a level ${floor(death * (13 / 12) + 1)} Foul Viper whose attacks inflict poison on enemies for 1...5...6 seconds."
shortDescription = "Spell: Exploit the nearest corpse to create a level 1...13...16 Viper Egg minion. When Viper Egg dies, it is replaced by a level 1...13...16 Foul Viper whose attacks inflict poison on enemies for 1...5...6 seconds."
icon = "Textures/Skills/Animate Viper Egg.png"
profession = PROFESSIONINDEX_NECROMANCER
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 3000
recharge = 8000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectSummonMinion
effectTarget = SkillTargetNone
targetType = SkillTargetTypeNone

local function getDeadActor(source)
  local deadActors = source:GetDeadActorsInRange(range)
  for i, actor in ipairs(deadActors) do
    if (actor:IsRecycleable()) then
      return actor
    end
  end
  return nil
end

function canUse(source, target)
  local actor = getDeadActor(source)
  if (actor == nil) then
    return SkillErrorInvalidTarget
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local actor = getDeadActor(source)

  if (actor == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  source:FaceObject(actor)
  return SkillErrorNone
end

function onSuccess(source, target)
  local actor = getDeadActor(source)

  if (actor == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(actor) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  if (actor:IsDead() == false) then
    return SkillErrorInvalidTarget
  end
  if (actor:IsRecycleable() == false) then
    return SkillErrorInvalidTarget
  end

  local attribVal = source:GetAttributeRank(ATTRIB_DEATH)
  local level = math.floor(attribVal * (13 / 12) + 1)
  source:SpawnSlave("/scripts/actors/npcs/viper_egg.lua", SLAVE_KIND_MINION, level, actor:GetPosition())
  actor:SetRecycleable(false)
  return SkillErrorNone
end

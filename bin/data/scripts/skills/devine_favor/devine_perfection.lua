-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5201
name = "Divine Perfection"
attribute = ATTRIB_DEVINE_FAVOUR
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: You gain ${floor(devinefavour * (1 / 12) + 0.5)} to all your Monk attributes. This ends if your Health falls below 100%."
shortDescription = "Enchantment Spell: You gain +0...1...2 to all your Monk attributes. This ends if your Health falls below 100%."
icon = "Textures/Skills/Divine Perfection.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 250
recharge = 20000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectIncAttributes
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function canUse(source, target)
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self:Index(), TIME_FOREVER)
  return SkillErrorNone
end

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5100
name = "Apprentice's Disaster"
attribute = ATTRIB_DOMINATION
skillType = SkillTypeHex
isElite = true
description = "Elite Hex: For 10 seconds, target foe casts spells 33% faster, but takes ${floor(domination * ((130 - 45) / 12) + 45)} damage every time they cast a spell."
shortDescription = "Elite Hex: For 10 seconds, target foe casts spells 33% faster, but takes 45...130...140 damage every time they cast a spell."
icon = "Textures/Skills/Apprentice's Disaster.png"
profession = PROFESSIONINDEX_MESMER
soundEffect = ""
particleEffect = ""

-- Domination  0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19   20   21
-- Damage     45    .    .    .    .    .    .    .    .    .    .    .  130    .    .  140    .    .    .    .    .    .

costEnergy = 10
costAdrenaline = 0
activation = 2000
recharge = 20000
overcast = 0
hp = 0
range = RANGE_CASTING
damageType = DAMAGETYPE_CHAOS
effect = SkillEffectDamage
effectTarget = SkillTargetTarget
targetType = SkillTargetTypeFoe

function canUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:GetId() == target:GetId()) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  return SkillErrorNone
end

function onStartUse(source, target)
  local err = canUse(source, target)
  if (err ~= SkillErrorNone) then
    return err
  end

  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    return SkillErrorInvalidTarget
  end

  target:AddEffect(source, self:Index(), 10000)

  return SkillErrorNone
end

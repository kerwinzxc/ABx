-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5008
name = "Basalt Bastion"
attribute = ATTRIB_EARTH
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: For 10 seconds you cannot be interrupted, but you move ${floor(earth * (30 - 25) / 12) + 25}% more slowly."
shortDescription = "Enchantment Spell: For 10 seconds you cannot be interrupted, but you move 50...30...25% more slowly."
icon = "Textures/Skills/Basalt Bastion.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 10
costAdrenaline = 0
activation = 1000
recharge = 30000
overcast = 5
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_EARTH
effect = SkillEffectCounterInterrupt
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function onStartUse(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (source:GetId() == target:GetId()) then
    -- Can not use this skill on self
    return SkillErrorInvalidTarget
  end
  if (source:IsEnemy(target) == false) then
    -- Targets only enemies
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  if (self:IsInRange(target) == false) then
    -- The target must be in range
    return SkillErrorOutOfRange
  end
  source:FaceObject(target)
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self:Index(), 10000)
  return SkillErrorNone
end

-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5015
name = "Silver Armour"
attribute = ATTRIB_EARTH
skillType = SkillTypeEnchantment
isElite = true
description = "Elite Enchantment Spell: For 20 seconds, every time you are the target of an enemy skill, up to ${floor(earth * (8 / 12))} adjacent foes are struck for ${floor(earth * ((45 - 8) / 12) + 8)} earth damage."
shortDescription = "Elite Enchantment Spell: For 20 seconds, every time you are the target of an enemy skill, up to 0...4...5 adjacent foes are struck for 8...45...48 earth damage."
icon = "Textures/Skills/Silver Armour.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 750
recharge = 30000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_EARTH
effect = SkillEffectDamage
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self:Index(), 20000)
  return SkillErrorNone
end

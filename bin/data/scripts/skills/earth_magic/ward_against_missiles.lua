-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5018
name = "Ward Against Missiles"
attribute = ATTRIB_EARTH
skillType = SkillTypeWardSpell
isElite = false
description = "Ward Spell: For ${earth * (20 - 5) / 12 + 5} seconds, create a Ward at your location. All non-spirit allies in the ward have a 50% chance to block missile attacks."
shortDescription = "Ward Spell: For 5...20...25 seconds, create a Ward at your location. All non-spirit allies in the ward have a 50% chance to block missile attacks."
icon = "Textures/Skills/Ward Against Missiles.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 15
costAdrenaline = 0
activation = 1000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectProtect
effectTarget = SkillTargetSelf | SkillTargetAoe
targetType = SkillTargetTypeNone

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  if (target == nil) then
    -- This skill needs a target
    return SkillErrorInvalidTarget
  end
  if (target:IsDead()) then
    -- Can not kill what's already dead :(
    return SkillErrorInvalidTarget
  end
  source:AddAOE("/scripts/actors/aoe/elementarist/ward_against_missiles.lua", self:Index(), self:GetPosition())

  return SkillErrorNone
end

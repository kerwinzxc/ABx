-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5021
name = "Glyph of Greater Concentration"
attribute = ATTRIB_ENERGY_STORAGE
skillType = SkillTypeGlypthe
isElite = true
description = "Elite Glyph: The next ${ceil(energystorage * (3 - 1) / 12 + 1)} spells you cast cannot be interrupted and ignore the dazed effect, and you cannot be knocked down while casting them. This glyph ends prematurely if you cast a spell while not enchanted."
shortDescription = "Elite Glyph: The next 1...3...4 spells you cast cannot be interrupted and ignore the dazed effect, and you cannot be knocked down while casting them. This glyph ends prematurely if you cast a spell while not enchanted."
icon = "Textures/Skills/Glyph of Greater Concentration.png"
profession = PROFESSIONINDEX_ELEMENTARIST
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 1000
recharge = 30000
overcast = 0
hp = 0

range = RANGE_CASTING
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectCounterInterrupt | SkillEffectCounterKD
effectTarget = SkillTargetSelf
targetType = SkillTargetTypeNone

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self:Index(), TIME_FOREVER)
  return SkillErrorNone
end

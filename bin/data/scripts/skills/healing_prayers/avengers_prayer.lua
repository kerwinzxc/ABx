-- By Koudelka

include("/scripts/includes/attributes.lua")
include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/damage.lua")

index = 5207
name = "Avenger's Prayer"
attribute = ATTRIB_HEALING
skillType = SkillTypeEnchantment
isElite = false
description = "Enchantment Spell: While you maintain this enchantment, every time target ally hits a foe, all of their party members in earshot healed for ${floor(healing * (7 / 12) + 0.5)} Health."
shortDescription = "Enchantment Spell: While you maintain this enchantment, every time target ally hits a foe, all of their party members in earshot healed for 0...7...10 Health."
icon = "Textures/Skills/Avenger's Prayer.png"
profession = PROFESSIONINDEX_MONK
soundEffect = ""
particleEffect = ""

costEnergy = 5
costAdrenaline = 0
activation = 2000
recharge = 20000
overcast = 0
hp = 0

range = RANGE_MAP
damageType = DAMAGETYPE_UNKNOWN
effect = SkillEffectHeal
effectTarget = SkillTargetParty
targetType = SkillTargetTypeNone

function canUse(source, target)
  return SkillErrorNone
end

function onStartUse(source, target)
  return SkillErrorNone
end

function onSuccess(source, target)
  source:AddEffect(source, self:Index(), TIME_FOREVER)
  return SkillErrorNone
end

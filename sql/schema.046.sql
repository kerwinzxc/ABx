INSERT INTO public.game_skills VALUES (public.random_guid(), 17, 'Mantra of Resolve', 'f7477bbe-50f5-11e8-a7ca-02100700d6f0', 1024, 0, 'Stance: You can not be interrupted for ${30 + (inspiration * 4)} seconds. You lose ${10 - (inspiration / 2)} energy when interrupt was prevented.', 'Stance: For 30..90 seconds prevents interrupts. Cost: lose 10..4 energy.', 'Textures/Skills/placeholder.png', '/scripts/skills/inspiration_magic/mantra_of_resolve.lua', '85d0939b-50f4-11e8-a7ca-02100700d6f0', '', '', 1, 0, 20000, 10, 0, 0, 0, 0);
UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects VALUES (public.random_guid(), 17, 'Mantra of Resolve', 1, '/scripts/effects/stance/matra_of_resolve.lua', 'Textures/Skills/placeholder.png');
UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 46 WHERE name = 'schema';
